function  xout= apply_signal_noise(xin,sn_thresh)
    
    // xin: un div avec 
    // xin.i: le nom de l'échantillon
    // xin.v: 3 variables:   m/z  signal    s/n
    
    // xout: un div avec 
    // xout.i: le nom de l'échantillon 
    // xout.v: 2 variables   m/z   signal
    
    // les lignes de xin dont s/n < sn_thresh sont supprimées de xout
    // la 3° colonne de s/n de xin est aussi supprimée dans xout
    
    
    // ex: xin.i=[ 'A';'A';'B';'C']; xin.d=[ 200 134 0.2; 200.5 13478  0.8; 458  4356 0.4; 382  4123 0.6 ];xin=div(xin);
    

    if sn_thresh>max(xin.d(:,3)) then
        error('the threshold is too high! No observation can be selected.')
    else 
        tri=find(xin.d(:,3)>=sn_thresh);
        xout=xin(tri,:);
        xout=xout(:,1:2);
    end
    
    
endfunction
