#!/bin/sh

### Run scilab providing the scilab script in $1 as standard input and passing
### the remaining arguments on the command line

# Function that writes a message to stderr and exits
fail()
{
    echo "$@" >&2
    exit 1
}

# Ensure scilab executable is found
#which scilab > /dev/null || fail "'scilab' is required by this tool but was not found on path"

# Extract first argument
infile=$1; shift

# Ensure the file exists
test -f $infile || fail "scilab input file '$infile' does not exist"

# Invoke scilab passing file named by first argument to stdin

if [[ "$OSTYPE" == "darwin16" ]];  then
scilab-cli -nb -noatomsautoload -f $* < $infile
elif [[ "$OSTYPE" == "linux-gnu" ]];  then
scilab601-cli -nwni  -f $* < $infile
fi
