function xfinal2=noemie2bis(x2,pas)
    
  
  // initialisation
  if argn(2)==1 then
      pas=0.125;      // valeur par défaut 
  end
  // x2.i contient les noms des échantillons 
  // les noms sont répétés pour chaque temps de rétention 
  // x2.d contient 2 colonnes: les temps de rétention et les intensités   
  
  
  // max/min des m/z
  max(x2.d(:,1))
  min(x2.d(:,1))
  
 
    
  // histogramme avec la valeur de pas 
  //figure;
  seuils_histo= [floor(min(x2.d(:,1))):pas:floor(max(x2.d(:,1)))+1];
  //nbre=histplot(seuils_histo,x2.d(:,1)); 
  nbre=histc(seuils_histo,x2.d(:,1));
  //h=gcf();
  //h.background=-2;
  //h.children.children.children.foreground=5;
  //pause
  // regroupements des m/z:
  groupes=[];
  gr1=0;
  gr2=0
  nbre=nbre';
  for i=1:max(size(nbre));   // 3706
      if nbre(i)~=0 & gr1==0 then 
          gr1=seuils_histo(i);
      end
      
      if nbre(i)==0 & gr1~=0 then
          gr2=seuils_histo(i);
          if groupes==[] then
              groupes=[gr1 gr2];
          else
              groupes=[groupes; [gr1 gr2]];
          end
          gr1=0;
          gr2=0; 
      end
   end
  
  

  // identification des différents échantillons 
  [obs, noms_obs, taille_obs]=str2conj(x2.i);
  nobs=max(size(noms_obs));


  // correction d'un bug 14mars17:
  x2.i=stripblanks(x2.i);
  noms_obs=stripblanks(noms_obs);


   // création du tableau final: 
   
   xfinal.d=[zeros(size(groupes,1),nobs)];
   
   for i=1:max(size(x2.i));
       numero_colonne=find(x2.i(i)==noms_obs); 
       numero_ligne=find(x2.d(i,1)>=groupes(:,1)& x2.d(i,1)<=groupes(:,2));
       if max(size(numero_ligne))>1 then 
           error('plusieurs numeros de ligne')
       elseif numero_ligne==[] then 
           disp(i,'pas de numero de ligne dans xfinal pour la ligne de x2.d:')
       else
           xfinal.d(numero_ligne,numero_colonne)=x2.d(i,2);
           numero_ligne=[];
       end
   end
   
   nbre_pas=(groupes(:,2)-groupes(:,1))/pas;
   
   
   xfinal2.d=[groupes nbre_pas xfinal.d];
   xfinal2.i=string(groupes(:,1))+'-' + string(groupes(:,2));
   xfinal2.v=['m/z_debut';'m/z_fin'; 'nbre de pas '; noms_obs];
   xfinal2=div(xfinal2);
   
   //figure;
   //curves(xfinal2(:,4:$))    


   
 endfunction  
   
 
