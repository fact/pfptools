function x_peaks_final=mz_find_ms2(res_merged,x836_mz,diff_rt0,diff_mz0,k)
    
    // res_merged: obtenu avec le wrapper merge_ms2 
    // res_merged.infos: une matrice de 3 colonnes (RT, mz_selected, signal) et en ligne 6 injections successives en deep scan
    // res_merged.infos.i: le n° d'injection en deepscan
    // res_merged.mzdata: une liste de même longueur que  
    // x836: les pics de référence ; un div avec 2 colonnes: mz puis RT 
    // k:identification des k plus proches voisins en fonction du m/z; par défaut: 1 
    //
    // x_peaks_final: un div 
    // x_peaks_final.i: les noms (.i) de x836_mz + #deepscan + le n° 

    x836_mz=div(x836_mz);
    
    if argn(2)<5 then
        k=1;
    end  
  
    if argn(2) <4 then
        diff_mz=0.0050;
    else
        diff_mz=diff_mz0;
    end
    
    if argn(2)<3 then
        diff_rt=0.25;
    else
        diff_rt=diff_rt0;
    end

    diff_mz2=diff_mz; 
    n=size(x836_mz.d,1);
    xout=[];

    for i=1:n;
        mz=res_merged.infos.d(:,2);    // mz selected 
        rt=res_merged.infos.d(:,1);    // rt selected 
        differ_mz=abs(mz-x836_mz.d(i,1));
        differ_tr=abs(rt-x836_mz.d(i,2)); 
        tri=find(differ_mz<diff_mz & differ_tr<diff_rt);
        if tri~=[] then 
            n_tri=max(size(tri));
            mz_selected=differ_mz(tri);
            [nul0,tri2]=gsort(mz_selected,'g','i');  //tri par m/z croissants
            tri2=tri2(1:min(max(size(tri2)),k),:);
            xtemp.d=res_merged.infos.d(tri,:);
            xtemp.d=xtemp.d(tri2,:);
            //pause
            xtemp.d=[ones(size(xtemp.d,1),1)*x836_mz.d(i,[2,1]) xtemp.d]; //17nov
            xtemp.i=res_merged.infos.i(tri);
            xtemp.i=xtemp.i(tri2);
            xtemp.i=x836_mz.i(i)+'#deepscan'+xtemp.i;
            xtemp.v=res_merged.infos.v;
            xtemp.v=['rt exp';'mz exp';'rt dpsc';'mz dpsc';'signal dpsc'] //17nov
            xtemp=div(xtemp);
        else
            xtemp.i=x836_mz.i(i); //+'#deepscan'+xtemp.i;
            xtemp.d=[x836_mz.d(i,[2,1]) (-1)*ones(1,3)];
            xtemp.v=['rt exp';'mz exp';'rt dpsc';'mz dpsc';'signal dpsc'];
            xtemp=div(xtemp);
        end
        if xout==[] then
            xout=xtemp;
        else   
            xout=[xout;xtemp];
        end
    end
       
    if xout~=[] then    
        xout=div(xout);   
    end;
    
    x_peaks_final=xout; 

    
endfunction




    
    
    
    
    
    
