function x_deisotoped=mz_11_pick_isotopes(x_isotopes,thresh_neutron,corr_coeff)
    
    
    // x_isotopes: une liste de longueur le nombre de RT (2443) 
    // chaque élément contient un div 
    
    // thresh_neutron:deux valeurs donnant les limites basse et haute de la masse d'un neutron 
    // ex: 0.0015 -> [1.0031-0.0015,1.0031+0.0015]
    
    // corr_coeff: coefficient de corrélation mini; 0;80 par defaut

    if  argn(2)<3 then
        corr_coeff=0.8;
    end

    n_i=size(x_isotopes);
    
    x_deisotoped=list();
    index_x_deiso=1;

    for i=1:n_i;
        x_i=x_isotopes(i);
        n_j=size(x_i.d,2);
        to_keep=zeros(n_j,1);
        for j=1:n_j-1;
            diff_mz_j_1n=abs(strtod(x_i.v)-strtod(x_i.v(j))-1.0031);                 // différence vecteur - valeur - 1 neutron
            diff_mz_j_05n=abs(strtod(x_i.v)-strtod(x_i.v(j))-1.0031/2);              // différence vecteur - valeur - 1 neutron
            
            diff_mz_j_1n(j)=max(diff_mz_j_1n);                        // neutralisation de la valeur à j qui vaut 0 à l'origine
            diff_mz_j_05n(j)=max(diff_mz_j_05n);
            
            index_min_1n=find(diff_mz_j_1n==min(diff_mz_j_1n));          // index du min; une seule valeur 
            index_min_05n=find(diff_mz_j_05n==min(diff_mz_j_05n));
            
            if max(size(index_min_1n))==1 & max(size(index_min_05n))==1 then 
            
                correl_j=abs(correl(x_i.d(:,j),x_i.d(:,j+1)));

                if (diff_mz_j_1n(index_min_1n)<thresh_neutron |diff_mz_j_05n(index_min_05n)<thresh_neutron) & correl_j>corr_coeff then  // pas isotope à j+1
                    to_keep(j)=1;
                    if diff_mz_j_1n(index_min_1n)<thresh_neutron then 
                        to_keep(index_min_1n)=1;
                    elseif diff_mz_j_05n(index_min_05n)<thresh_neutron then
                        to_keep(index_min_05n)=1;
                    end
                    //if j == index_min_1n | j == index_min_05n then
                    //   disp('j=index_min')
                    //end
                end
            end

        end

        // suppression des m/z neutralisés
        tri=find(to_keep==1);
        if tri ~=[] then
            x_i=x_i(:,tri);
            x_deisotoped(index_x_deiso)=x_i;
            index_x_deiso=index_x_deiso+1;
        end
       
    end
    
    
endfunction
