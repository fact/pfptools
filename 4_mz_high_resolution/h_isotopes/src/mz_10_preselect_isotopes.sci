function [x_isotopes,x_presence_isotopes]=mz_10_preselect_isotopes(list_rt,list_allmz,x_tr_mz4, corr_thresh)
    
    // list_rt: une liste qui regroupe les RT qui intersectent 

    [n0,q0]=size(x_tr_mz4.d);

    n=size(list_rt.d,1);
    
    x_scramble.d=zeros(n0,n);
    
    for i=1:n;
        x_i=list_rt.d(i,1);
        x_j=list_rt.d(i,2);
        x_temp=list_allmz(i);
        x_scramble.d(x_i:x_j,i)=x_temp.d;        // une seule colonne dans list_alllmz
        //disp(list_allmz.d(i))
    end
    
    
    x_scramble.i=x_tr_mz4.i;
    x_scramble.v=string(list_rt.d(:,6));
    x_scramble=div(x_scramble);

    // corrélations
    x_corrmat=corrmat(x_scramble,x_scramble);
    // neutralisation de la diagonale
    for i=1:size(x_corrmat.d,1);
        x_corrmat.d(i,i)=0;
    end
    
    x_isotopes=list();
    x_presence_isotopes=list();
    indice=1;
    flag=1;
    while flag==1;
        line_corrmax=max(x_corrmat.d,'r');
        val_max=find(line_corrmax==max(line_corrmax));
        val_max=val_max(1);             // si plusieurs solutions; on ne garde qu'une colonne
        tri=find(x_corrmat.d(:,val_max) > corr_thresh);
        if tri ~=[] then 
            x_isotopes(indice)=x_scramble(:,[val_max tri]);
            x_corrmat.d([val_max tri],:)=0;                 // neutralisation des m/z sélectionnées
            x_corrmat.d(:,[val_max tri])=0;
            indice=indice+1;
            //disp(indice,'indice=')
        else
            flag=0
        end
    end
    

    
endfunction
