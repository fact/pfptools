function  x_dw=mz_6bis_durbin_watson(x_tr_mz)
    
    // pas de calcul matriciel car pas assez de memoire
    // x_dw doit varier entre 0 et 2 
    // 0=ce qu on recherche
    
    [n,q]=size(x_tr_mz.d);
    
    // calcul de la norme des differences et des vect-colonne de x_tr_mz
    x_diff=x_tr_mz.d(2:n,:)-x_tr_mz.d(1:n-1,:);
    rmsce=zeros(q,1);
    rmsce2=zeros(q,1);
    for i=1:q;
        rmsce(i)=sqrt((x_diff(:,i)'*x_diff(:,i))/(n-1));
        rmsce2(i)=sqrt((x_tr_mz.d(:,i)'*x_tr_mz.d(:,i))/(n));
    end
    
    x_dw=rmsce./rmsce2;
    
    
endfunction
