function resbary_out=mz_1_merge_by_k(resbary_in,k)
    
    // regroupe les TR par k et ajoute les signaux si m/z égaux

    if ~isfield(resbary_in,'time') |  ~isfield(resbary_in,'mzdata') then
        error('fields time and/or mzdata are missing in resbary_in')
    end
 
    n=max(size(resbary_in.time));  
    n2=max(size(resbary_in.mzdata));
    if n~=n2 then
        disp('n time ~= n mzdata');
    end
    
    resbary_out.time=[];
    resbary_out.mzdata=list();
    
    index=1;
    i=1;
    while index <=n-k+1;
        resbary_out.time(i)=mean(resbary_in.time(index:index+k-1));
        // regroupement des k mzdata
        resbary_out.mzdata(i)=resbary_in.mzdata(index);
        for j=1:k-1;
            resbary_out.mzdata(i)=[resbary_out.mzdata(i);resbary_in.mzdata(index+j)]
        end;
        [nul,tri]=gsort(resbary_out.mzdata(i)(:,1),'g','i');
        resbary_out.mzdata(i)=resbary_out.mzdata(i)(tri,:);                   // tri par m/z croissants  
        resbary_out.mzdata(i)(:,1)=0.0001*round(10000*resbary_out.mzdata(i)(:,1));     // arrondi à 0.0001       // 1fev20
        
        // sommes des signaux pour m/z identiques         
        resbary_mz=resbary_out.mzdata(i)(:,1);
        resbary_mz=unique(resbary_mz);
        n_i=max(size(resbary_mz));
        resbary_signal=zeros(n_i,1);
        for j=1:n_i;
            tri_temp=find(resbary_out.mzdata(i)(:,1)==resbary_mz(j));
            resbary_signal(j)= sum(resbary_out.mzdata(i)(tri_temp,2));        // somme des signaux de même m/z         
        end;
        resbary_out.mzdata(i)=[resbary_mz resbary_signal];
 
        // suite de la boucle
        index=index+k;
        i=i+1;
    end;

    
endfunction
