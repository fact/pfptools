function resbary_out=mz_1_out_of_k(resbary_in,k)
    
    // extrait 1 TR sur k

    if ~isfield(resbary_in,'time') |  ~isfield(resbary_in,'mzdata') then
        disp('fields time and/or mzdata are missing in resbary_in')
    end
    
    n=max(size(resbary_in.time));
    
    list_to_keep=[1:k:n]
    
    n2=max(size(list_to_keep));
    
    resbary_out.time=resbary_in.time(list_to_keep,:);
    
    resbary_out.mzdata=list();
    
    for i=1:n2;
        resbary_out.mzdata(i)=resbary_in.mzdata(list_to_keep(i));   
    end
    
    
endfunction
