<tool id="2021_05_eics_centroids" name="MS .h5 or .dat -> MS-centroid" version="0.0.1">

<description>    </description>

<requirements>
    <requirement type="package" version="6.1.1">scilab</requirement>
</requirements>


<stdio>
    <exit_code range="1:" level="fatal" />
</stdio>


<command> 
 <![CDATA[ 
 $__root_dir__/packages/scilab-*/bin/scilab-cli -nb -quit -f ${script_file} 
 ]]>
</command>

  <configfiles>
    <configfile name="script_file">
    <![CDATA[ exec("$__tool_directory__/src/mz_1bis_signal_threshold.sci",-1); ...
        exec("$__tool_directory__/src/mz_2_barycenter_adjust2.sci",-1); ...
        exec("$__tool_directory__/src/mz_2_savgol2.sci",-1); ...
        exec("$__tool_directory__/src/mz_3_remove_zeros.sci",-1); ...
        exec("$__tool_directory__/src/mz_eic5_step1.sci",-1); ...
        exec("$__tool_directory__/src/mz_hdf5tosci.sci",-1); ...
        if ~isdef('pls') then ...
            atomsSystemUpdate(); ...
            atomsInstall('FACT'); ...
            atomsLoad('FACT'); ...
        end; ...
        lasterror(); ...
        type_sm=${type_sm}; ...
        format_io=${format_io}; ...
        if type_sm==1 then           ...    // mode profil Orbitrap
            if format_io==0 then     ...    // format Scilab
                load("${filein}");   ...    // la variable doit s'appeler res
            elseif format_io==1 then ...    // format HDF5
                res=mz_hdf5tosci("${filein}"); ...
            end; ...
            resbary=mz_eic5_step1(res,${signalthresh}); ...
        elseif type_sm==0 then      ...    // mode centroide
            if format_io==0 then    ...    // format Scilab = on ne fait rien 
                load("${filein}");  ...    // la variable doit deja s'appeler resbary
            elseif format_io==1 then ...   // format HDF5
                res=mz_hdf5tosci("${filein}"); ...
                resbary=mz_3_remove_zeros(res); ...
            end; ...
        end; ...
        save("${resbary}",'resbary'); ...
        if ~isempty(lasterror(%f)) then ...
        write(0,lasterror()); ...
        end; ]]>
</configfile>
  </configfiles>


  <inputs>
    <param name="format_io" type="select" size="5" format="integer" label="MS format" >
        <option value="0" > Scilab format .dat </option>
        <option value="1" selected="true"  > HDF5 format .h5 </option>
    </param>
    <param name="filein"       format="mat,h5,dat"                type="data"      label="MS data"    />
    <param name="signalthresh" size="5"         value="0"  type="integer"   label="Baseline threshold"  help="if not applied during the spectrum acquisition"  />
    <param name="type_sm" type="select" size="5" format="integer" label="Mode during spectrum acquisition" help="if centroid: 0 are removed; if profile: centroids are calculated" >
        <option value="0" selected="true" > Centroid </option>
        <option value="1"   > Profile mode with Orbitrap </option>
    </param>
  </inputs>


  <outputs>
    <data name="resbary"    format="mat"   label="MS-centroid"  >
    </data>
  </outputs>
  
  
   <tests>
   
    <test>
       <param name="format_io"    value="0"/>
       <param name="filein"       value="VI2016_AC_4Areduit_1.dat"/>
       <param name="signalthresh" value="0"/>
       <param name="type_sm" value="1"/>
       <output name="resbary"   file="VI2016_AC_4Areduit_1centroide.mat" compare="sim_size" delta="2000" />
    </test>

    <test>
       <param name="format_io"    value="1"/>
       <param name="filein"       value="VI2016_AC_4Areduit_1.h5"/>
       <param name="signalthresh" value="0"/>
       <param name="type_sm" value="1"/>
       <output name="resbary"   file="VI2016_AC_4Areduit_1centroide.mat" compare="sim_size" delta="2000" />
    </test>

  </tests>
  
  
  <help>


**Author**  Jean-Claude Boulet (INRAE).


---------------------------------------------------

====================
CENTROID CALCULATION
====================

-----------
Description
-----------


Centroids are calculated according to the reference cited below. 


------
Inputs
------

**MS format**

Two formats are possible: HDF5 obtained from Scilab (.dat) or HDF5 obtained from MSconvert then HDFView (.h5)

The Scilab format contains a structure with two fields: 

- time: the retention times, a vector;

- mzdata: a list of same length as time; each element of the list is a matrix of two columns representing a MS1 mass spectrum: m/zvalues and associated signals.

The HDF5 (.h5) file contains at least the following fields: 

- ChomatogramTime: a vector containing the retention times of each mass spectrum. 

- ChromatogramIndex: the indexes of the signals identified in ChomatogramTime; the last value of ChromatogramIndex is the size of ChomatogramTime.

- ChromatogramIntensity: the sum of intensities of the mass spectra associated to the RTs of ChomatogramTime; ChomatogramTime and ChromatogramIntensity are vectors of same length.

- SpectrumIndex: a vecteur of integers corresponding to the numbers of signals of each MS in SpectrumMZ et SpectrumIntensity. For example, if the two first values of SpectrumIndex are 1421 and 2887, then the first MS is between 0 à 1421, the second between 1422 and 2887. The last value of spectrumIndex is the number of values in SpectrumMZ and in SpectrumIntensity.

- SpectrumMz: the m/z values. For each mass spectrum, the first value is the real m/z value, but the following are obtained by the sum of the previous values. For example, if the three first values are: 220.0501, 0.1171 and 0.9856, then the real m/z values to be calculated will be: 220.0501, 220.1672=220.0501+0.1171, 220.2528=220.0501+0.1171+0.9856.

- SpectrumIntensity: a vector containing all the intensities recorded.

Even if the MS spectrum has been obtained with the centroid option of MSconvert, this function should be applied to remove all the zeros kept by the MSconcert peak picking option. 


**MS data**

The series of mass spectra 


----------
Parameters
----------


**Baseline threshold**

Signals under the threshold are dropped. Default value = 0. 

Most of the peaks should be separated by regions with signal=0. 


**Mode during spectrum acquisition**

Profile or centroid.



-------
Outputs
-------

**centroid MS data**

For each RT, the signals associated to the same m/z value have been replaced by their centroid. 

The output is a structure with two fields: 

- time: retention times;

- mzdata: m/z values and centroid signals.


</help>


<citations>

<citation type="bibtex">@article{sgcentroid,
   title={High-resolution mass spectrometry (HRMS): focus on the m/z values estimated by the Savitzky-Golay first derivative},
   author={Boulet, J.C. and Meudec, E. and Vallverdu-Queralt, A. and Cheynier, V.},
   journal={Rapid Communications in Mass Spectrometry},
   year={2020},
   doi={10.1002/rcm.9036}
   }
</citation> 

</citations>


</tool>
