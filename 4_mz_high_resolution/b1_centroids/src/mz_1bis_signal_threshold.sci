function res2=mz_1bis_signal_threshold(res,sigthresh);
    
    res2=res;
    
    n=max(size(res2.time));
    
    for i=1:n;
        resi=res2.mzdata(i);
        
        resi_col2=resi(:,2);
        
        // application du seuil
        resi_col2(resi_col2<sigthresh)=0;
        
        resi(:,2)=resi_col2;
        
        res2.mzdata(i)=resi;
    end
    
    
    
endfunction
