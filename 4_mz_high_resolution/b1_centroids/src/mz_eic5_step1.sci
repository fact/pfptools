function resbary=mz_eic5_step1(res,sigthresh)
     
    // cette fonction mobilise les fonctions suivantes:
    //  exec('mz_1bis_signal_threshold.sci',-1);
    //  exec('mz_2_barycenter_adjust2.sci',-1);   
    //  exec(‘mz_2_savgol2.sci’,-1);   

    // Arguments:
    // res: une variable avec 2 champs: time et mzdata
    // sigthresh: un seuil pour un seul signal sur un seul TR ; ex: sigthresh=10000
    
    // resbary: un fichier barycentrique 

    
    res=mz_1bis_signal_threshold(res,sigthresh);
    
    resbary=mz_2_barycenter_adjust2(res);   


endfunction

























