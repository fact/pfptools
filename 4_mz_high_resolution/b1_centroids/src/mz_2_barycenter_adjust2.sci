function xout=mz_2_barycenter_adjust2(xin)
    
    // remplace les donnees brutes par: 
    // (1) mz barycentrique
    // (2) somme des signaux

    // xin: une structure avec 2 champs: 
    // xin.time
    // xin.mzdata
    
    // xout: 2 champs, comme xin
    
    // re-ecrit totalement et simplifie le 9dec20
    
    xout.time=xin.time;
    xout.mzdata=list();
    n=max(size(xin.time));    

    for i=1:n;
       xtemp_in=xin.mzdata(i);
       [nul,xtemp_out]=mz_2_savgol2(xtemp_in);
       xout.mzdata(i)=xtemp_out;
    end 
  
    
    
    
endfunction
