function x_out=mz_3_remove_zeros(x_in)
    
    // x_in: un fichier au format barycentrique avec 2 champs: .time et .mzdata 
    // si le calcul centroide a été fait par MSconvert, le fichier contient des m/z avec signal=0: on va les enlever 
    
    // initialisation
    x_out.time=x_in.time;
    x_out.mzdata=list();
    
    n=max(size(x_in.time));

    for i=1:n;
        mzdata_i=x_in.mzdata(i);
        index=find(mzdata_i(:,2)>0);
        x_out.mzdata(i)=mzdata_i(index,:);
    end
    
endfunction
