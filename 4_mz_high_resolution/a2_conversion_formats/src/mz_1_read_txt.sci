function [res]= mz_1_read_txt(xfichier, pas )
    
     // lecture d'un fichier texte, obtenu par exportation d'un fichier .mzML 

     // xfichier: le nom du fichier texte  ex: xfichier -> '151123-vinrose_acet3%_T5h_cpl-pos.txt'
     // pas:      le pas d'acquisition des données: pas=10 -> 1/10, pas = 1 -> 1/1 = toutes 
     //           pas est un multiple de 10  
     // res:      une structure 
     
     // ex: res=mz_read_txt('151123-vinrose_acet3%_T5h_cpl-pos.txt',2);
    
     // compatibilité version 5.5.2 et 6.0.0
     version=getversion();
     version=strsplit(version,'-');
     version=version(2);
     version=part(version,1);
     version=strtod(version);
     if version < 6 then
         stacksize('max')
     end

     // pas par défaut: pas =1 / on garde toutes les données
     if argn(2)==1 then
         pas=1;
     end

     // ouverture du fichier:
     id=mopen(xfichier,'rt');
     
     // initialisation:
     i=1;
     x_out=list();
     x_masses=[];
     x_time=[];
     end_file=0;
     list_index=1;

     // début de la boucle while, pour collecter les données de masse
     while end_file==0;
 
         line=mgetl(id,1);
         line=stripblanks(line)  //enlève les blancs de début et fin, pas les blancs intermédiaires
         
         if meof(id)==0 & line~=[] then      // on continue... 
             
             // temps départ de l'acquisition: 
             if regexp(line,'/scan start time/')~=[] then 
                 [a]=strsplit(line,',');
                 time=strtod(a(2));         
             end 

             // arrêt de boucle à la fin des spectres de masse:
             if (i<=10*pas) | (x_time($-1)<x_time($))  then   // 10*pas pour avoir des valeurs dans x_time 

                // extraction des données: 
                if regexp(line,'/binary:/')~=[]   then 
                   
                    a=strsplit(line,']');
                    xtemp=stripblanks(a(2));     // extrait les données (1 x 1) et enlève les blancs de début et de fin 
                    xtemp=strsplit(xtemp,' ');   // met xtemp sous forme d'un vecteur vectical de chaines de caractères 
                    xtemp=strtod(xtemp);         // mise en alpha-numérique 
                    
                    xtemp=xtemp(isnan(xtemp)==%F);  // suppression des nan rajoutés accidentellement en fin de fichier xtemp, 16mars17
                    
                    if x_masses==[] then  
                        x_masses=xtemp;       
                    else
                        if size(x_masses,1)==size(xtemp,1) then
                            x_masses=[x_masses xtemp];
                        else
                            disp('erreur de dimensions ligne 73 de mz_read_txt' )
                            disp(size(x_masses),'size(x_masses)=')
                            disp(size(xtemp),'size(x_temp)=')
                            pause
                            clear x_masses
                        end

                    end
                end
  
    
                // sélection d'un spectre de masse et d'une unité de temps par unité de pas: 
                if size(x_masses,2)==2 then 
                    if floor(i/pas)==ceil(i/pas) then // on en garde 1/pas 
                        if x_time==[] then
                            x_time=time;
                        else
                            x_time=[x_time;time];
                        end
                        x_out(list_index)=x_masses;
                        //disp(size(x_out(list_index)),'size(x_out(list_index))=')    
                        list_index=list_index+1;
                    end
                    //disp(i,'i=')
                    //disp(list_index,'list_index=')
                    x_masses=[];
                    i=i+1;
                end
             else  // ligne 53
                end_file=1;
             end  // ligne 53
         
         else  // ligne 44 
             //end_file=meof(id)    // on arrête la boucle while 
             end_file=1;
         end  // ligne 44

     end  // ligne 39
    
     mclose(id)
     
     res.time=x_time;
     if size(res.time,1)==1 then
         res.time=res.time';
     end
     res.mzdata=x_out;
     
    
    
endfunction
