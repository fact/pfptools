function x_mz=mz_hdf5nantestosci(file_in,cvparam_txt,max_scans)
    
    // entrees: -------------------------
    // file_in: un fichier au format md5 (HDF5) du format de BIBS-Nantes
    //      rappel: mzxml de Nantes -> md5 avec msconvert 
    // cvparam_txt: l'export en txt de CVParam avec HDFView; un fichier texte
    // max_scans: valeur introduite pour accelerer la fonction de test
    
    // sorties: ---------------------------
    // x_mz: une variable avec deux champs:
    // x_mz.time
    // x_mz.mzdata une liste dont chaque élément est un vecteur de 2 colonnes: m/z et intensités de signal
    
    // JCB 2 aout18
    
    
    
    // lecture du fichier HDF5 ----------------------------------
    a=h5open(file_in,'a');
    //h5dump(a.root.CVParam)  
    //spectrum_time=h5read(a, "/CVParam",[1 22],[1 1]);    
    spectrum_intensity=h5read(a.root.SpectrumIntensity);  // intensite du signal; un vecteur (1 x 4E08)
    spectrum_mz=h5read(a.root.SpectrumMZ);                // m/z; un vecteur (1 x 4E08)
    spectrum_index=h5read(a.root.SpectrumIndex);          // index d'identification des spectres 
                                                          // dans les variables précédentes
                                                          // ex: 112166 227841 -> 1° spectre de 1 à 112166
                                                          //                      2° spectre de 112167 à 227841
                                                          //                      etc  
    h5close(a)
     
    if argn(2)<3 then
        nbr_scans=max(size(spectrum_index));
    else
        nbr_scans=max_scans;
    end
 
    // 1- reconstruction du spectre --------------------------------------------
    time=spectrum_index(1:nbr_scans);
    mzdata=list();
    
    for i=1:nbr_scans;
        if i==1 then 
            plage_i=[1,double(spectrum_index(i))];
        else 
            plage_i=[double(spectrum_index(i-1))+1,double(spectrum_index(i))];
        end
        
        // 2- extraction et reconstruction des m/z
        // dans le fichier HDF5, ne sont enregistrées que les différences de m/z entre 2 mesures
        mz_i=spectrum_mz(plage_i(1):plage_i(2));
        
        n=size(mz_i,2);
        mz_i2=zeros(1,n);
        
        mz_i2(1)=mz_i(1);
        for j=2:n;
            mz_i2(j)=mz_i2(j-1)+mz_i(j);
        end
        
        // 3- extraction des signaux
        sig_i=spectrum_intensity(plage_i(1):plage_i(2));

        // regroupement des m/z et signaux
        sp_i=[mz_i2;sig_i];
        
        mzdata(i)=sp_i';

        
    end
    
    
    // 2- Reconstruction des temps de retention --------------------------------

    // lecture du fichier
    a=mopen(cvparam_txt);
    txt=mgetl(a)
    mclose(a)

    // tri des lignes + extraction des RT
    n=size(txt,1);
    x_rt=[];
  
    for i=1:n;
                x_temp=strsplit(txt(i,:),'	18');        // valeur 18 trouvee avec tab avant
                if max(size(x_temp))>1 then
                    x_temp2=stripblanks(x_temp(1));
                    x_temp2=strtod(x_temp2);
                    if x_rt==[] then
                        x_rt=x_temp2;
                    else
                        x_rt=[x_rt;x_temp2];
                    end
                end
    end

    // 3- prepaaration de la sortie --------------------------------------------
    x_mz.time=x_rt';
    x_mz.mzdata=mzdata;
    
    
endfunction
