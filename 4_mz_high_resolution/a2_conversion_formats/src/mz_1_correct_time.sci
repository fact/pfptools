function xout=mz_1_correct_time(xin,pas)
    
    // entrée:  xin.time
    //          xin.mzdata
    // sortie:  xout.time
    //          xout.mzdata
    // but: corriger le dernier point d'acquision qui est la somme de touts les RT avec Orbitrap 
    
    // pas: pour ne pas garder tous les temps               // modifie le 23avril19
    
    n=max(size(xin.time));
    
    indexes=[1:pas:n-1]; 
    
    n_i=max(size(indexes));
    
    xout.time=xin.time(indexes);
    xout.mzdata=list();
    for i=1:n_i;
        xout.mzdata(i)=xin.mzdata(indexes(i));
    end


endfunction
