function [res]= mz_1_read_txt_ms1_ms2(xfichier, ms_level)
    
     // lecture d'un fichier texte, obtenu par exportation d'un fichier .mzML 

     // xfichier: le nom du fichier texte  ex: xfichier -> '151123-vinrose_acet3%_T5h_cpl-pos.txt'
     // ms_level: 1 (MS1) ou 2 (MS2)

     // res:      une structure 
     
     // ex: res=mz_read_txt('151123-vinrose_acet3%_T5h_cpl-pos.txt',2);
    


     
     // initialisation:
     i=1;
     x_out=list();
     x_masses=[];
     x_time=[];
     x_selected_ion=[];
     x_signal_sel_ion=[];
     
     end_file=0;
     list_index=1;
     flag2=1;
     

     mclose('all')   // si fichier resté ouvert lorsd'une session précédente      
     
     // ouverture du fichier:
     id=mopen(xfichier,'rt');
     
     // début de la boucle while, pour collecter les données de masse  
     while meof(id)==0 & flag2==1;
        line=mgetl(id,1);
        line=stripblanks(line)  //enlève les blancs de début et fin, pas les blancs intermédiaires
         
        if line~=[] & flag2==1 then      // on étudie la ligne 
            
            if grep(line,'chromatogramList')~=[] | grep(line,'electromagnetic')~=[] |   grep(line,'TIC')~=[]  then    // 7nov20: on a fini le MS2 - on arrête là 
               flag=0;
               flag2=0;
            end 
             
            if regexp(line,'/ms level/')~=[] & flag2==1 then   // on identifie un nouveau spectre 
                 
                [a]=strsplit(line,',');
                mslevel=strtod(a(2));  
             
                if mslevel==ms_level then;   // on va garder ce spectre 
                         
                    // initialisation des sorties
                    time=-1;
                    selected_ion=-1;
                    signal_sel_ion=-1;
                    x_masses=[];
                     
                    flag=1;
                    while flag==1 & flag2==1;     //23nov
                        line=mgetl(id,1);
                        line=stripblanks(line); 
                        if line ~=[] then 
                                 
                            if flag~=0 & flag2~=0 then  
                                if regexp(line,'/scan start time/')~=[] then 
                                    a=strsplit(line,',');
                                    time=strtod(a(2));         
                                end 
                                if regexp(line,'/selected ion/')~=[] then 
                                    a=strsplit(line,',');
                                    selected_ion=strtod(a(2));         
                                end 
                                if regexp(line,'/peak intensity/')~=[] then 
                                    a=strsplit(line,',');
                                    signal_sel_ion=strtod(a(2));         
                                end 

                                // extraction des données: 
                                if regexp(line,'/binary:/')~=[]   then 
                                    a=strsplit(line,']');
                                    xtemp=stripblanks(a(2));     // extrait les données (1 x 1) et enlève les blancs de début et de fin 
                                    xtemp=strsplit(xtemp,' ');   // met xtemp sous forme d'un vecteur vectical de chaines de caractères 
                                    xtemp=strtod(xtemp);         // mise en alpha-numérique 
                                    xtemp=xtemp(isnan(xtemp)==%F);  // suppression des nan rajoutés accidentellement en fin de fichier xtemp, 16mars17
                                    // le fichier x_masses doit contenir 2 colonnes: m/z et signal 
                                    if x_masses==[] then  
                                        x_masses=xtemp;       
                                    else
                                        if size(x_masses,1)==size(xtemp,1) then
                                            x_masses=[x_masses xtemp];
                                        else
                                            ('erreur de dimensions ligne 78 de mz_read_txt' )
                                            (size(x_masses),'size(x_masses)=')
                                            (size(xtemp),'size(x_temp)=')
                                        end
                                    end
                                end
  
                                if regexp(line,'/defaultArrayLength/')~=[] then  // on passe au spectre suivant 
                                    if size(x_masses,2)==2 then 
                                        if x_time==[] then
                                            x_time=time;
                                            if ms_level==2 then
                                                x_selected_ion=selected_ion;
                                                x_signal_sel_ion=signal_sel_ion;
                                            end
                                        else
                                            x_time=[x_time;time];
                                            if ms_level==2 then
                                                x_selected_ion=[x_selected_ion;selected_ion];
                                                x_signal_sel_ion=[x_signal_sel_ion;signal_sel_ion];
                                            end
                                        end;
                                        x_out(list_index)=x_masses;
                                        list_index=list_index+1;
                                    else
                                        error('error in the dimension of x_masses')
                                    end    
                                    flag=0;  // on a fini pour ce spectre    
                                end
                            end
                        end
                    end              
                end
            end
        end
    end

    
    mclose(id)
     
    res.time=x_time;
     if size(res.time,1)==1 then
         res.time=res.time';
     end
     res.mzdata=x_out;
     if ms_level==2 then
         res.mz_targetted=x_selected_ion;
         res.mz_targetted_signal=x_signal_sel_ion;
     end
     
    
    
endfunction
