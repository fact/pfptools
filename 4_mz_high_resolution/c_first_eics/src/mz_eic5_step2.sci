function x_tr_mz3=mz_eic5_step2(resbary,diff_mz_min,signal_thresh,durb_wats)

    // avant 8jan20: function [x_tr_mz3,resbary]=mz_eic5_step2(resbary,diff_mz_min,signal_thresh,durb_wats)

    // fonction qui suit: mz_eic5_step1
    
    // cette fonction mobilise les fonctions suivantes:
    //  exec('mz_3_extract_all_mz2.sci',-1);
    //  exec('mz_4_scan_mz_raw6.sci',-1);
    //  exec('mz_5_tr_matrix6bis.sci',-1);
    //  exec('mz_6bis_durbin_watson.sci',-1)
    //  exec('mz_6_rescan_mz2.sci',-1)
    //  exec('mz_7_dwcleaning.sci',-1)

    // Arguments:
    // resbary: le SM au format barycentrique = centroide 
    // diff_mz_min: seuil de m/z; ex: 0,0024
    // signal_thresh: un signal minimum regroupant plusieurs signaux sur le même TR; ex: 30000 sur vin   
    // durb_wats:   Durbin_Watson; on recherche durb_wats=0; ex de seuil: 1      

    
    // extraction de tous les m/z de resbary
    allmz=mz_3_extract_all_mz2(resbary); 


    // tri de la liste de m/z  
    allmz2=mz_4_scan_mz_raw6(allmz,diff_mz_min,signal_thresh);
    clear allmz;   //8jan20


    // calcul de la matrice x_tr_mz3:
    // avant 8jan20: [x_tr_mz,calculs_stats_tr_mz]=mz_5_tr_matrix6bis(resbary,allmz2,diff_mz_min,signal_thresh);
    x_tr_mz=mz_5_tr_matrix6bis(resbary,allmz2,diff_mz_min,signal_thresh);
    clear resbary allmz2; // 8jan20
    
        
    x_tr_mz2=mz_6_rescan_mz2(x_tr_mz,1.2*diff_mz_min);
        clear x_tr_mz ; // 8jan20
    
    
    x_tr_mz3=mz_7_dwcleaning(x_tr_mz2,durb_wats);


    
endfunction

























