function x_tr_mz3=mz_7_dwcleaning(x_tr_mz2,durbwats)


    // cette fonction nettoie la matrice des EICs avec DW trop élevé 
        
    // x_tr_mz2: matrice EIC issue de la fonction mz_6_rescan_mz2
        
    // durbwats:    ex: dw=1.2
       


    // nettoyage avec Durbin-Watson: très efficace
    res_dw=mz_6bis_durbin_watson(x_tr_mz2);


    tri=find(res_dw<durbwats);

    if tri~=[] then
        x_tr_mz3=x_tr_mz2(:,tri);   // 1000*= rapide de sélectionner les bonnes colonnes qu'enlever les mauvaises
    else
        x_tr_mz3=x_tr_mz2;
    end
    
    // note: nettoyage avec eic_signal testé avec valeur=10000 -> inutile!  


        
endfunction
