function allmz=mz_3_extract_all_mz2(resbary0)
    
    // extraction des m/z et des signaux totaux
    
    // resbary0: une structure avec les champs .time et .mzdata
    
    // allmz: une matrie ce deux colonnes: m/z et signal total  
    
    n=max(size(resbary0.time));
    
    allmz=resbary0.mzdata(1);
    
    for i=2:n;
        allmz=[allmz;resbary0.mzdata(i)];
    end
    
    // arrondis 
    allmz(:,1)=round(10000*allmz(:,1))/10000;
    
    // tri des donnees
    [nul,tri]=gsort(allmz(:,1),'g','i');
    allmz=allmz(tri,:);
    
    // regroupememt des valeurs identiques
    n2=max(size(allmz));
    for i=2:n2;
        if allmz(i-1,1)==allmz(i,1) then
            allmz(i,2)=allmz(i,2)+allmz(i-1,2);  // rajout des signaux
            allmz(i-1,1)=0;
        end
    end
    
    // suppression des valeurs nulles
    tri=find(allmz(:,1)~=0);
    allmz=allmz(tri,:);
    
endfunction
