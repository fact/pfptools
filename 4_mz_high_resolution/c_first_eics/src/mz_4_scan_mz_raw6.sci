function mzall2=mz_4_scan_mz_raw6(mzall0,diff_mz_min,signal_thresh)
    
    // trie des valeurs de m/z 
    
    // mzall0: une matrice de 2 colonnes: (1) une liste des valeurs de m/z (2) les signaux associés
    // diff_mz_min: un seuil en dessous duquel deux m/z proches sont regroupees; par défaut: 0,0005
    // signal_thresh: un niveau minimum de signal total; par defaut: 30000
    
    // mzall2: une matrice de 2 colonnes avec m/z et signaux
    
    if argn(2)<3 then
        signal_thresh=30000;
    end
    
    if argn(2)<2 then
        diff_mz_min=0.0005;
    end
    
    
    // 2° regroupement
    mzall1=mzall0;
    mzall2=mzall0;
    
    n=max(size(mzall1));
    
    signal_max=max(mzall1(:,2));
    index=0;
    flag=0;
    
    while (signal_max > signal_thresh) & (max(size(mzall1))>2) & flag==0 ;
        //if index/10000==round(index/10000) then
        //    disp(index,'index=')
        //    disp(signal_max,'signal_max=')
        //end //--------------------------------------
                    
        signal_max=max(mzall1(:,2));
        index_max_n=find(mzall1(:,2)==signal_max);  // il peut y avoir plusieurs solutions!
        n2=max(size(index_max_n));
        for i=1:n2;
            index_max=index_max_n(i);
            indexes_plage=find( (mzall1(:,1)>mzall1(index_max,1)-diff_mz_min) & (mzall1(:,1)< mzall1(index_max,1)+diff_mz_min));
            mzall2(indexes_plage,:)=0;
            mzall2(index_max,:)=mzall1(index_max,:);
            mzall1(indexes_plage,:)=0; 
        end

        tri=find(mzall2(:,1)~=0);

        if tri==[] then 
            flag=1;
        else   
            if index/100==round(index/100) then  // nettoyage periodique pour alleger les donnees
                mzall1=mzall1(tri,:);
                mzall2=mzall2(tri,:);
            end
        end
        index=index+1;
    end
    
    // suppression des valeurs nulles residuelles
    tri=find(mzall2(:,1)~=0);
    mzall2=mzall2(tri,:);
    

    
endfunction
