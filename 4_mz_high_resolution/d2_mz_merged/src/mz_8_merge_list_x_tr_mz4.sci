function [merged_x_tr_mz4file,pmille_coupe]=mz_8_merge_list_x_tr_mz4(list_eic4_files)
    
    //list_x_tr_mz4: une liste de divs x_tr_mz4 => .d =même nbre de m/z et .i=TR , pas le même nombre
    //mz4_stats: un div => .i = les m/z et .v = 5 variables: mz 1=min 2=max 3=moyen 4=moyen-2ET 5=moyen+2ET
    
    
    // calcul du nbre de TR minimum et maximum
    n_files=size(list_eic4_files);
    
    nbr_tr_min=max(size(list_eic4_files(1).i));
    nbr_tr_max=nbr_tr_min;
    
    for i=2:n_files;
        nbr_temp=max(size(list_eic4_files(i).i));
        if nbr_temp < nbr_tr_min then 
            nbr_tr_min=nbr_temp;
        end 
        if nbr_temp > nbr_tr_max then 
            nbr_tr_max=nbr_temp;
        end
    end
    
    // somme des répétitions 
    merged_x_tr_mz4file.v=list_eic4_files(1).v;
    temp_i=list_eic4_files(1).i;
    merged_x_tr_mz4file.i=temp_i(1:nbr_tr_min);   // on coupe à n_tr_min

    temp_d=list_eic4_files(1).d;
    merged_x_tr_mz4file.d=temp_d(1:nbr_tr_min,:);
    for i=2:n_files;
        temp_d=list_eic4_files(i).d;
        merged_x_tr_mz4file.d=merged_x_tr_mz4file.d + temp_d(1:nbr_tr_min,:);
    end
    
    // sortie
    merged_x_tr_mz4file=div(merged_x_tr_mz4file);
    
    
    pmille_coupe=1000*(nbr_tr_max-nbr_tr_min)/nbr_tr_min;
    
  
    
endfunction
