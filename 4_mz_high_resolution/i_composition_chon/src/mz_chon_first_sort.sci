function list_first_sorted=mz_chon_first_sort(res_mw_list,deltamz_max,OCmin,OCmax,HCmin,HCmax)
    
    // entrees:
    // res_mw_list est un div issu de la sortie de la fonction: mw2formulae_list
    //     "       contient 8 colonnes: mz obs / MW calc / nbre C / nbre H / nbre O / nbre N / delta mz / rang 
    
    OsurC=res_mw_list.d(:,5)./res_mw_list.d(:,3);
    HsurC=res_mw_list.d(:,4)./res_mw_list.d(:,3);
    
    // tri decompose en plusieurs etapes
    n=size(res_mw_list.d,1);
    to_keep=ones(n,1);                      // on mettra 0 pour les valeurs a eliminer 
    
    // critere O sur C
    index=find(OsurC<OCmin);
    to_keep(index)=0;
    index=find(OsurC>OCmax);
    to_keep(index)=0;
    
    // critere H sur C
    index=find(HsurC<HCmin);
    to_keep(index)=0;
    index=find(HsurC>HCmax);
    to_keep(index)=0;    
    
    // critere delta mz min 
    index=find(abs(res_mw_list.d(:,7))>deltamz_max);
    to_keep(index)=0;
    
    // critere final de tri
    index=find(to_keep==1);
    
    list_first_sorted=res_mw_list(index,:);
    
    
endfunction
