function [res_form]=mw2formulae_v4(x0,n_hplus,n_electron,thresh_choice,threshold,limit,base_calc,path)
    
    // propose des formules brutes CHON a partir d'une masse 
    // x0:           masse brute observee (corrigée du H+)
    // threshold:   ecart-maximum de x a la valeur de reference
    // limit:       nombre maximum de formules proposees 
    // base_calc:   pour calculer la base (1) ou ne pas la calculer (0)
    // path:        le chemin où se trouve le fichier x_mw2formulae_base.dat
 
    // ex de test: [a,b]=mw2formulae(865.1980,0.001,5);  doit donner: C=45 H=37 O=18 N=0
    
    // la fonction compare x a une valeur tiree d'une base, qui contient les masses theoriques calculees dans une 
    // certaine fourchette de C, H et O.
  
    // pour calculer la base: mettre flag=0
   
    if  argn(2)<2 then
        n_hplus=1;
    end
    
    if argn(2)<3 then
        n_electron= n_hplus;
    end
   
    if argn(2)<4 then 
        thresh_choice='mz';        // 1 = delta m/z     2 = delta ppm
    end 

    if argn(2)<5 then                  // 31jan20
        if thresh_choice=='mz' then 
              threshold=0.001;
        elseif thresh_choice=='ppm' then 
              threshold=2;
        end 
    end

    if argn(2)<6 then
        limit=10;
    end
   
    if argn(2)<7 then
        flag=1;      // on ne recalcule pas la base
    else
        flag=base_calc;
    end

    // pour recalculer la base avec une valeur nulle
    if x0<1 then
       flag=0;
    end

    if argn(2)<8 then
        path='';
    end
    
    
    // construction d'une base: max. 250 C, 500 H, 250 O, 2 N [compter quelques minutes]
    // valeur exacte pour C12
    //    "     "     "   H1  (www.chemcalc.org)
    //    "     "     "   O16 (www.chemcalc.org)
    //    "     "     "   N14 (www.chemcalc.org)
    
    
    n_c=200;
    n_h=400;
    n_o=200;
    n_n=1;    // il y a zero aussi 
    m_c=12;                 
    m_h=1.0078250321;       
    m_o=15.9949146196;        
    m_n=14.0030740049;  
        
    if flag==0 then        
        base=zeros(n_c,n_h,n_o,n_n+1);
        for i=1:n_c;
            for j=1:n_h;
                for k=1:n_o;
                    for l=1:(n_n)+1;
                        base(i,j,k,l)=i*m_c + j*m_h + k*m_o+ (l-1)*m_n;
                    end
                end
            end
        end
        save(path+'x_mw2formulae_base.dat','base');
    end

    load(path+'x_mw2formulae_base.dat');
    
    // ----------------------------------------------------------------
    
    // correction de la masse de l'electron (qui manque) 18oct17
    
    m_electron=0.0005486;

    
    
    
    // obtention des solutions 
    
    n_masses=max(size(x0));
    
    res_form.d=[];
    res_form.i=[];
    res_form.v=[];
    
    for k=1:n_masses;
        
        if x0(k)> 15000 then
            error('m/z too high for the database')
        end

        if n_hplus(k)~=0 then   // maj 20jan22
	        x=abs(n_hplus(k)) * x0(k) - n_hplus(k)* m_h - n_electron*m_electron; 
        elseif n_hplus(k)==0 then
            x=x0(k)- n_electron*m_electron;
        end;
    
      
        // critère de sélection : m/z ou ppm   31jan20
        if thresh_choice=='mz' then
            [a,b,c,d]=find(abs(base -x)<threshold);
        elseif thresh_choice=='ppm' then
            [a,b,c,d]=find(abs(base -x)<threshold*0.000001*x);
        end

        
        if c~=[] then
            res_sol=[a' b' c' (d'-1)];    // car premier tableau = pas de N = zéro N 
        else
            break
        end
    
        // calcul des differences entre x et la base
        n=size(res_sol,1);
        diffs=zeros(n,1);
        for i=1:n;
            base_i=base(res_sol(i,1),res_sol(i,2),res_sol(i,3),res_sol(i,4)+1);
            diffs(i)=x-base_i;
        end
    
    
    
        // tri selon les valeurs decroissantes
        diffs2=abs(diffs);
        [values,indices]=gsort(diffs2,'r','i');

  
        // mise en forme des donnees  
        res_d=[n_hplus*ones(n,1) x*ones(n,1) res_sol(indices,:) diffs(indices,:) [1:1:n]'];
        res_d=res_d(1:min([n limit]),:);
        
        res_i=string(x0(k)*ones(n,1));
        res_i=res_i(1:min([n limit]),:);

        if res_form.d==[] then
           res_form.d= res_d; 
           res_form.i= res_i; 
        else
           res_form.d=[res_form.d;res_d] ;   
           res_form.i=[res_form.i;res_i];
        end 
    
    end

    res_form.v=['nbr of H+';'mw calc';'C';'H';'O';'N';'delta (obs-calc)';'rank'];

    if res_form.d ~=[] & res_form.i ~= [] then
        res_form=div(res_form);  
    else 
        res_form.d=ones(1,8)*(-1);
        res_form.i=x0;
        res_form=div(res_form); 
    end

    
endfunction
