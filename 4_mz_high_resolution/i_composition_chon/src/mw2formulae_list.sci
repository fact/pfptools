function res_mw_list= mw2formulae_list(list_mz,azote,path)
    
    // list_mz = un div avec: 
    //         list_mz.i = liste de mz 
    //         list_mz.v = nbre H+ / mz or ppm / thresholds
    // azote: presence d'azote (1) ou absence d'azote (0)   // enleve car ici c'est avec N   //31jan20
    // path: le chemin où est x_mw2formulae_base      // 31jan20

    // res_mw_list = un div avec: numero mz / valeur mz / mw calc / C /H / O / N 
       
       
    if argn(2)<3 then 
        path='';
    end
    
    if argn(2)<2 then
        azote=1;  
    end    
        
    n=size(list_mz.d,1); 
    
    res_mw_list.v=['m/z obs';'MW calcule';'nbr C';'nbr H';'nbr O';'nbr N'; 'delta m/z';'rank'];
    res_mw_list.d=[];
 
    for i=1:n;                          // pour chaque composé= chaque ligne de list_mz
        mz_i=strtod(list_mz.i(i));
        h_i=list_mz.d(i,1);
        thresh_choice_i=list_mz.d(i,2);    // 31jan20
        if thresh_choice_i==1 then 
            threshchoice='mz';
        elseif thresh_choice_i==2 then 
            threshchoice='ppm';
        end;
        thresh_i=list_mz.d(i,3);

        if azote==1 then
            mw_i=mw2formulae_v4(mz_i,h_i,threshchoice,thresh_i,1000,1,path);         // 1000 pour ne pas être limite en nbre de formules; 1 pour ne pas recalculer la base
        elseif azote==0 then
            mw_i=mw2formulae_v4_0N(mz_i,h_i,threshchoice,thresh_i,1000,1,path);
            // rajout d'une colonne N=0 pour recaler
            if mw_i~=[] then
                mw_i.v=[mw_i.v(1:5);'N';mw_i.v(6:$)];
                mw_i.d=[mw_i.d(:,1:5) zeros(size(mw_i.d,1),1) mw_i.d(:,6:$)];
            end
        end

        ni=size(mw_i.d,1);

        if ni==0 then               // mw_i=[] -> pas trouvé de correspondance 
            mw_i.d=[zeros(8,1)];
            mw_i.i=list_mz.i(i);
        mw_i=div(mw_i);


        if res_mw_list.d==[] then
            res_mw_list.d=[strtod(mw_i.i)  mw_i.d(:,2:8)];
            res_mw_list.i=string(i*ones(ni,1));
        else
            res_mw_list.d=[res_mw_list.d;[strtod(mw_i.i)  mw_i.d(:,2:8)]];
            res_mw_list.i=[res_mw_list.i;string(i*ones(ni,1))]
        end
    end
    res_mw_list=div(res_mw_list);
    
    //arrondi 
    res_mw_list.d(:,7)=0.00001*round(100000*res_mw_list.d(:,7))   
    
    if azote==0 then
        tri=find(res_mw_list.d(:,6)==0);    // on ne garde que les solutions sans N  
        res_mw_list=res_mw_list(tri,:);
        res_mw_list=cdel(res_mw_list,6);
    end        
    
    
endfunction
