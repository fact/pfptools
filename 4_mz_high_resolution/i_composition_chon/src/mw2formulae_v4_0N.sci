function [res_form]=mw2formulae_v4_0N(x0,n_hplus,n_electron,thresh_choice,threshold,limit,base_calc,path)
    
    // propose des formules brutes CHON a partir d'une masse 
    // x0:           masse brute observee (corrigée du H+)
    // threshold:   ecart-maximum de x a la valeur de reference
    // limit:       nombre maximum de formules proposees 
    // base_calc:   pour calculer la base (1) ou ne pas la calculer (0)
 
    // ex de test: [a,b]=mw2formulae(865.1980,0.001,5);  doit donner: C=45 H=37 O=18 N=0
    
    // la fonction compare x a une valeur tiree d'une base, qui contient les masses theoriques calculees dans une 
    // certaine fourchette de C, H et O.
  
    // pour calculer la base: mettre flag=0
   
    if  argn(2)<2 then
        n_hplus=1;
    end
    
    if argn(2)<3 then
        n_electron= n_hplus;
    end
   
    if argn(2)<4 then 
        thresh_choice='mz';        // 1 = delta m/z     2 = delta ppm
    end 
   
    if argn(2)<5 then                  // 31jan20
        if thresh_choice=='mz' then 
              threshold=0.001;
        elseif thresh_choice=='ppm' then 
              threshold=1.5;
        end 
    end

    if argn(2)<6 then
        limit=10;
    end
   
    if argn(2)<7 then
        flag=1;      // on ne recalcule pas la base
    else
        flag=base_calc;
    end

    // pour recalculer la base avec une valeur nulle
    if x0<1 then
       flag=0;
    end

    if argn(2)<8 then
       path='';
    end


    // construction d'une base: max. 250 C, 500 H, 250 O, 2 N [compter quelques minutes]
    // valeur exacte pour C12
    //    "     "     "   H1  (www.chemcalc.org)
    //    "     "     "   O16 (www.chemcalc.org)
    //    "     "     "   N14 (www.chemcalc.org)
    
    n_c=250;
    n_h=500;
    n_o=250;
    //n_n=1;

    m_c=12;                 
    m_h=1.0078250321;       
    m_o=15.9949146196;        
    m_n=14.0030740049;  
        
    if flag==0 then
        base=zeros(n_c,n_h,n_o);
        for i=1:n_c;
            for j=1:n_h;
                for k=1:n_o;
                    //for l=1:(n_n)+1;
                        base(i,j,k)=i*m_c + j*m_h + k*m_o;
                    //end
                end
            end
        end
        save(path+'x_mw2formulae_base_0N.dat','base');
    end;

    load(path+'x_mw2formulae_base_0N.dat');

    // ----------------------------------------------------------------
    
    // correction de la masse de l'electron (qui manque dans le H+)
    
    m_electron=0.0005486;

    
    // obtention des solutions 
    
    n_masses=max(size(x0));
    
    res_form.d=[];
    res_form.i=[];
    res_form.v=[];
    
    for k=1:n_masses;
        
        if n_hplus(k)~=0 then
            //x=abs(n_hplus(k)) * x0(k) - n_hplus(k)* (m_h - m_electron);  // modifie le 1 avril 19 //annule 22mars20
	        x=abs(n_hplus(k)) * x0(k) - n_hplus(k)* m_h - n_electron*m_electron;  // modifie le 1 avril 19 
        elseif n_hplus(k)==0 then
            x=x0(k)- n_electron*m_electron; //22mars20
        end;
        
        // critère de sélection : m/z ou ppm   31jan20
        if thresh_choice==1 | thresh_choice=='mz' then
            [a,b,c,d]=find(abs(base -x)<threshold);
        elseif thresh_choice==2 |thresh_choice=='ppm' then
            [a,b,c,d]=find(abs(base -x)<threshold*0.000001*x);
        end
    
        if c~=[] then
            res_sol=[a' b' c' ];    
        else
            break
        end
    
        // calcul des differences entre x et la base
        n=size(res_sol,1);
        diffs=zeros(n,1);
        for i=1:n;
            base_i=base(res_sol(i,1),res_sol(i,2),res_sol(i,3));
            diffs(i)=x-base_i;
        end
    
        // tri selon les valeurs decroissantes
        diffs2=abs(diffs);
        [values,indices]=gsort(diffs2,'r','i');

  
        // mise en forme des donnees  
        res_d=[n_hplus*ones(n,1) x*ones(n,1) res_sol(indices,:) diffs(indices,:) [1:1:n]'];
        res_d=res_d(1:min([n limit]),:);
        
        res_i=string(x0(k)*ones(n,1));
        res_i=res_i(1:min([n limit]),:);

        if res_form.d==[] then
           res_form.d= res_d; 
           res_form.i= res_i; 
        else     
           res_form.d=[res_form.d;res_d] ;   
           res_form.i=[res_form.i;res_i];
        end
    
    end

    res_form.v=['nbr of H+';'mw calc';'C';'H';'O';'delta (obs-calc)';'rank'];


    if res_form.d ~=[] & res_form.i ~= [] then
        res_form=div(res_form);  
    else 
        res_form.d=ones(1,7)*(-1);
        res_form.i=x0;
        res_form=div(res_form);  
    end

    
endfunction
