function res_mw_list= mw2formulae_list_0N(list_mz, path)
    
    // list_mz = un div avec: 
    //         list_mz.i = liste de mz 
    //         list_mz.v = nbre H+ / thresholds
    // path: le chemin où est x_mw2formulae_base      // 31jan20
  
    
    // res_mw_list = un div avec: numero mz / valeur mz / mw calc / C /H / O  
        
        
    n=size(list_mz.d,1); 
    
    res_mw_list.v=['m/z obs';'MW calcule';'nbr C';'nbr H';'nbr O'; 'delta m/z';'rank'];
    
    for i=1:n;
        mz_i=strtod(list_mz.i(i));
        h_i=list_mz.d(i,1);
        thresh_choice_i=list_mz.d(i,2);    //31jan20
        thresh_i=list_mz.d(i,3);
        mw_i=mw2formulae_v4_0N(mz_i,h_i,thresh_choice_i,thresh_i,100,1);         // 100 pour ne pas être limite en nbre de formules; 1 pour ne pas recalculer la base
//pause
        ni=size(mw_i.d,1);
        
        if i==1 then         
            res_mw_list.d=[strtod(mw_i.i)  mw_i.d(:,2:7)];
            res_mw_list.i=string(i*ones(ni,1));
        else
            res_mw_list.d=[res_mw_list.d;[strtod(mw_i.i)  mw_i.d(:,2:7)]];
            res_mw_list.i=[res_mw_list.i;string(i*ones(ni,1))]
        end
    end
    res_mw_list=div(res_mw_list);
    
    //arrondi du delta m/z
    res_mw_list.d(:,6)=0.00001*round(100000*res_mw_list.d(:,6))
    
    
    
    
endfunction
