function labels_out=extract_strings(labels_in,indexes)
    
    // cette fonction extrait des caracteres dans une liste de caracteres 
    
    // labels_in:  un vecteur de caracteres de dimensions (n,1) ou (1,n)
    // labels_out: un vecteur de caracteres de dimensions (n,1)
    
    // indexes: ex : ‘1,4:6,8’ => 1 4 5 6 8
   
    // vecteur mis verticalement
    if size(labels_in,1)==1 then
        labels_in=labels_in';
    end
    
    // initialisation
    labels_out=[];
    
    // extraction
    n=size(labels_in,1);
    
    for i=1:n;
        execstr(msprintf('label_temp=part(labels_in(i),[%s]);',indexes));
        if i==1 then 
            labels_out=label_temp;
        else
            labels_out=[labels_out;label_temp];
        end
    end
    
    
    // labels_out: un vecteur de caracteres de dimensions (n,1)
    
    
    
    
endfunction
