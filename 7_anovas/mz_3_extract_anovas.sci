function [xout,pvalues,res_anova]=mz_3_extract_anovas(xin0,codes_ech,p_max)
    
    // objectif: extraire les m/z dont les différences sont significatives entre échantillons
    
    // xin: variable de sortie de mz_2_compare_mz_signals; un div
    // attention: observations en lignes, variables en colonnes donc transposer
    
    // codes_ech: les codes des échantillons (groupes, familles etc)
    
    // p_max: la proba max
    if argn(2)<3 then
        p_max=0.05;
    end
    
    xin=xin0;
    
    xin=xin(:,4:$);    // on enlève les 3 premières lignes 
    xin=xin';
    
    // réalisation de l'anova:
    res_anova=snk(xin,codes_ech);
    
    n_obs=max(size(codes_ech));
    n_ech=max(size(unique(codes_ech)));
    n_variables=max(size(xin.v));
    
    fischer_vect=res_anova.fvalues.d;
    ddl_a=n_ech-1;
    ddl_r=n_obs-n_ech;
    
    // extraction des p-values
    [nul,pvalues]=cdff('PQ',fischer_vect,ddl_a*ones(n_variables,1),ddl_r*ones(n_variables,1));

    tri=find(pvalues<p_max);
    
    if tri==[] then
        xout=[];
    else 
        xout=xin0(tri,:);      // on re-rajoute les 3 lignes enlevees au debut 
    end
    
    
endfunction
