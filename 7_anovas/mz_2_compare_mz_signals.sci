function xfinal2=mz_2_compare_mz_signals(x_allmz,pas)
    
    // ex fichier baptise: noemie2.sci 
    
    // x_allmz:    x_allmz.d contient 2 colonnes: m/z et signal 
    //             x_allmz.i contient les noms des observations
  
    // initialisation
    if argn(2)==1 then
        pas=0.125;      // valeur par défaut 
    end
  
    // max/min des m/z
    max(x_allmz.d(:,1))
    min(x_allmz.d(:,1))
  
 
    
    // histogramme avec la valeur de pas 
    seuils_histo= [floor(min(x_allmz.d(:,1))):pas:floor(max(x_allmz.d(:,1)))+1];
    nbre=histc(seuils_histo,x_allmz.d(:,1));
    groupes=[];
    gr1=0;
    gr2=0
    nbre=nbre';
    for i=1:max(size(nbre));   // 3706
        if nbre(i)~=0 & gr1==0 then 
            gr1=seuils_histo(i);
        end
      
        if nbre(i)==0 & gr1~=0 then
            gr2=seuils_histo(i);
            if groupes==[] then
                groupes=[gr1 gr2];
            else
                groupes=[groupes; [gr1 gr2]];
            end
            gr1=0;
            gr2=0; 
        end
    end

    // identification des différents échantillons 
    [obs, noms_obs, taille_obs]=str2conj(x_allmz.i);
    nobs=max(size(noms_obs));

    // correction d'un bug 14mars17:
    x_allmz.i=stripblanks(x_allmz.i);
    noms_obs=stripblanks(noms_obs);

    // création du tableau final: 
    xfinal.d=[zeros(size(groupes,1),nobs)];
   
    for i=1:max(size(x_allmz.i));
        numero_colonne=find(x_allmz.i(i)==noms_obs); 
        numero_ligne=find(x_allmz.d(i,1)>=groupes(:,1)& x_allmz.d(i,1)<=groupes(:,2));
        if max(size(numero_ligne))>1 then 
            error('plusieurs numeros de ligne')
        elseif numero_ligne==[] then 
            disp(i,'pas de numero de ligne dans xfinal pour la ligne de x_allmz.d:')
        else
            xfinal.d(numero_ligne,numero_colonne)=x_allmz.d(i,2);
            numero_ligne=[];
        end
    end
   
 //   nbre_pas=(groupes(:,2)-groupes(:,1))/pas;
   
//    xfinal2.d=[groupes nbre_pas xfinal.d];
//    xfinal2.i=string(groupes(:,1))+'-' + string(groupes(:,2));
//    xfinal2.v=['m/z_debut';'m/z_fin'; 'nbre de pas '; noms_obs];
//    xfinal2=div(xfinal2);
    
    // 25mars19: valeurs + precises de m/z 
    n_xf=size(groupes,1);
   
    mz_min_max=[];
    for i=1:n_xf;
        tri=find(x_allmz.d(:,1)>=groupes(i,1) & x_allmz.d(:,1)<=groupes(i,2));
        plage_mz=x_allmz.d(tri);
        if mz_min_max==[] then 
            mz_min_max=[min(plage_mz) max(plage_mz)];
        else 
            mz_min_max=[mz_min_max; [min(plage_mz) max(plage_mz)]];
        end
    end
   
    delta=mz_min_max(:,2)-mz_min_max(:,1);
    delta=ceil(delta*10000)/10000;
   
    xfinal2.d=[mz_min_max delta xfinal.d];
    xfinal2.i=string(mz_min_max(:,1))+'-'+string(mz_min_max(:,2));
    xfinal2.v=['m/z_debut';'m/z_fin'; 'delta m/z'; noms_obs];
    xfinal2=div(xfinal2);
    
   
 endfunction  
   
 
