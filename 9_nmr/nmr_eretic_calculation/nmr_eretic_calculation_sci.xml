<tool id="V24.03_eretic_calculation_sci" name="Quantification" version="0.0.1">
  <description>using Eretic  </description>

<requirements>
    <requirement type="package" >scilab</requirement>
  </requirements>

  <stdio> 
    <exit_code range="1:" level="fatal" />
  </stdio>  

 <command><![CDATA[ 
     if [ -d $__root_dir__/packages/scilab-2024.0.0 ]; then $__root_dir__/packages/scilab-2024.0.0/bin/scilab-cli -nb -quit -f ${script_file}; else $__root_dir__/packages/scilab-2024.0.0/bin/scilab-cli -nb -quit -f ${script_file}; fi 
]]>
 </command>


  <configfiles>
    <configfile name="script_file"> <![CDATA[ 

      funcprot(0);  // enlève les messages
      exec("$__tool_directory__/src/glx_nmr_eretic_calculation.sci",-1);
      lasterror(%f);    

      dilution_factor=glx_tab2div("${X_dilution.file_name}");
      col_dilution=${Col_dilution}-1;
      dilution_factor=dilution_factor(:,col_dilution);
      
      // ajustement selon le mode de calcul = valeurs >1 ou <1
      // la fonction glx_nmr_eretic_calculation est basée sur des coeffs >1
      if mean(dilution_factor.d)<1 then
        n_temp=size(dilution_factor.d,1);
        dilution_factor.d=ones(n_temp,1)./dilution_factor.d;
      end
    
      compound_area0=glx_tab2div("${Compound_area.file_name}");
      col_area=${Col_area}-1;
      compound_area=compound_area0(:,col_area);
      
      compound_nbH=${Compound_nbH};
      compound_mw=${Compound_mw};
    
      qs_concentration=${Qs_concentration};
      qs_area=${Qs_area};
      qs_nbH=${Qs_nbH};
      qs_mw=${Qs_mw};
      
      compound_concentration = glx_nmr_eretic_calculation(compound_area,compound_mw,compound_nbH,dilution_factor,qs_area,qs_concentration,qs_mw,qs_nbH);

      // modification des labels 
      compound_concentration.v='[Conc] ' + compound_concentration.v; 
    
      // rajout des infos de cosinus associees a l'aire
      x_out=[compound_area0 compound_concentration];
    
      // sorties
      div2tab(x_out,"${X_out}");
      
      if ~isempty(lasterror(%f)) then
        write(0,lasterror());
      end;  ]]>
    </configfile>
  </configfiles>


  <inputs>
      
      <param name="X_dilution"  format="tabular"  type="data" label="Dilution  coefficient" help=" issued from the sample preparation"/>
      <param name="Col_dilution" type="data_column"  data_ref="X_dilution"  numerical="true"  multiple="False" use_header_names="true" force_select="true" label="Column of the dilution coefficients"/>
      
      <param name="Compound_area"  format="tabular"  type="data" label="Compound areas" help="mesured after deconvolution" />
      <param name="Col_area" type="data_column"  data_ref="Compound_area"  numerical="true"  multiple="False" use_header_names="true"  force_select="true" label="Column of the areas"/>
      
      <param name="Compound_nbH" value="1"  type="integer"  min="1" max="10" label="Compound - number of H" help="corresponding to the area measured" />
      <param name="Compound_mw" value="100"  type="float"  label="Compound - molecular weight" help="malic 134.08 lactic 90.08" />

      <param name="Qs_concentration"  value="100"  type="float"  label="QS - concentration"/>
      <param name="Qs_area"           value="100"  type="float"  label="QS -area"/>
      
      <param name="Qs_nbH"      value="1"  type="integer"  min="1" max="10" label="QS - number of H" help="corresponding to the area measured" />  
      <param name="Qs_mw"       value="100"  type="float"  label="QS - molecular weight" help="citric 192.12"/>
      
  </inputs>


  <outputs>
    <data name="X_out" format="tabular" label="Concentrations and quality criteria" />
  </outputs>
  
  
  <tests>
    <test>
      <param name="Xdata" value="x_simufilters_x10disp2.tabular"/>
      <param name="Xtarget" value="x_simufilters_x10ft2.tabular"/>
      <param name="width" value="4" />
      <output name="SF">
      <assert_contents> 
          <has_text text="0.169896"/>
      </assert_contents>
       </output>
    </test>
  </tests>
<help>

.. class:: warningmark

**TIP:** If your data is not TAB delimited, use *Convert format data Tools-&gt;Convert*

.. class:: infomark

**Author** Jean-Claude Boulet (INRAE)


---------------------------------------------------

===========================
QUANTIFICATION USING ERETIC
===========================

-----------
Description
-----------

The Eretic method is processed to quantify a compound from NMR samples. 

All spectra, from samples, reference compounds or QS, are supposed to have been acquired in the same conditions, and particularily with the same number of datapoints per spectrum, and the same ppm range. 

Thus the formula has been simplified and requires less inputs. 

-----------
Input files
-----------
**Dilution coefficient**

A vector of dimensions (n x 1) containing the dilution rate of each sample during the preparation process. 
For example, if 1 vol. of buffer was added to 9 vol. of wine, the dilution rate is (1+9)/9=1.11. The inverse 9/(1+9)=0.9 is also accepted, provided that all dilution coefficients have been calculated in the same way.

**Dilution column**

The column of Dilution coefficients containing the coefficients

**Compound area**

The matrix or vector containing the compound areas, issued from the function Deconvolution and area integration. 

**Area column**

The column of Compound area containing the areas

**Compound - number of H**

According to the range of ppm which has been integrated to calculate the areas.

**Compound - molecular weight**

**QS - concentration**

calculated in the NMR tube.

**QS - area**

Obtained with the function QS-integrate. Same unit as Compound area.

**QS - number of H**

According to the range of ppm which has been integrated to calculate the areas.

**QS - molecular weight**


------------
Output files
------------

**Concentrations**

Calculated for each sample and a single reference compound associated to an integration range; ex. malic acid 2.73-2.95.
    
The file contains also several validation criteria.




</help>


<citations>

</citations>


</tool>
