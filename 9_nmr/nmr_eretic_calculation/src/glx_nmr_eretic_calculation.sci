
function compound_concentration = glx_nmr_eretic_calculation(compound_area,compound_mw,compound_nbH,compound_dilution,standard_area,standard_concentration,standard_mw,standard_nbH)
    
    // calcule la concentration avec la formule Eretic 
    
    // compound_area:               un vecteur (n x 1)  [div]
    // compound_mw:                 un réel 
    // compound_nbH:                un entier
    // compound_dilution:           facteur de dilution, <=1
    // standard area:               un réel 
    // standard_concentration       concentation massique dans le tube
    // standard_mw:                 un réel
    // standard_nbH:                un entier 

    
    compound_concentration.d=standard_concentration *(compound_mw/standard_mw)*(standard_nbH/compound_nbH)*(compound_area.d .* compound_dilution.d) / standard_area;
    
    compound_concentration.i=compound_area.i;
    compound_concentration.v= compound_area.v;
    
    compound_concentration=div(compound_concentration);
    
    
endfunction
