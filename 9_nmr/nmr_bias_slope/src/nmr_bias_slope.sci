        function xout=nmr_bias_slope(xin,Kbias_slope)


        // xin est un div de dimensions (n x q)   n observations sur q variables

        // Kbias_slope est un div de dimensions (nk x 2)    nk=plages à corriger  2 = pente + biais 
        

        xout=xin;

        // nombre de champs avec biais-pente déterminés
        nk=size(Kbias_slope.i,1); 
       
        for i=1:nk;
          tri=find(xin.v==Kbias_slope.i(i));
          xout.d(:,tri)=(xout.d(:,tri)+Kbias_slope.d(i,2))*Kbias_slope.d(i,1);
        end
        
        xout.d(xout.d<0)=0;
        
        endfunction
        
       
