function  [x_out]=glx_nmr_qs_integrate(x0,regions0)
    
    // intégration de une ou plusieurs zones définies préalablement pour chaque composé du QS
    
    // x0:                          une matrice (n x q)
    // regions0:                    une matrice (k x 2 mini)       // k = nbre de composés X nbre de regions par compose
    //                                                             // 2 = valeurs de ppm de debut et de fin de region a integrer
    
    // x_out:                       un vecteur (k x 2 mini)        //  1° colonne = valeurs d'integration 
                                                                   //  2° colonne = ratio corrigé / non corrigé
    
    x_in=div(x0);                                // n x q
    x_regions=div(regions0);                     // k x 2
    
    // inversion pour avoir les ppm dans l'ordre croissant
    // x_in=x_in(:,$:-1:1);     // 21mars24
    
    // si x_in=vecteur, le mettre en ligne
    if size(x_in.d,2)==1 then
        x_in=x_in';
    end;
    
    [n,q]=size(x_in.d);
    k=size(x_regions.d,1);
    
    // ---------------------------------
    // transformation des ppm en indices
    // ---------------------------------
    
    ppm=strtod(x_in.v);   

    indexes=zeros(k,2);
    for i=1:k;
        ppm_diff1=abs(ppm-ones(q,1)*x_regions.d(i,1)');      // matrice (q x k)
        ppm_diff2=abs(ppm-ones(q,1)*x_regions.d(i,2)');      // matrice (q x k)
        
        res_find1=find(ppm_diff1==min(ppm_diff1));
        res_find2=find(ppm_diff2==min(ppm_diff2));
        indexes(i,1)=res_find1(1);
        indexes(i,2)=res_find2(1);
    end;
    
    // ---------------------------
    // ajustement de ligne de base
    // ---------------------------
    
    areas_all=zeros(k,3);           // 3 colonnes pour: integration avant/pendant/après la zone
    areas_corrected=zeros(k,1);
    areas_not_corrected=zeros(k,1);
    
    n_indexes=indexes(:,2)-indexes(:,1)+1;          // (k x 1)
    
    
    for i=1:k;
        sum_temp_1=sum(x_in.d(:,indexes(i,1)-50:indexes(i,1)+50-1))/n;
        sum_temp_2=sum(x_in.d(:,indexes(i,1):indexes(i,2)))/n;
        sum_temp_3=sum(x_in.d(:,indexes(i,2)+1:indexes(i,2)+50))/n;
        // calcul d'aire corrige du nbre de points differents pour ldb avant et apres zone
        areas_corrected(i,1)=sum_temp_2-((sum_temp_1+sum_temp_3)* (indexes(i,2)-indexes(i,1)+1)/(50+50));
        areas_not_corrected(i,1)=sum_temp_2;
    end;
    
    ratio=areas_corrected ./ areas_not_corrected;

    // 7mars24
    ratio=0.0001*round(10000*ratio);

    x_out.d=[areas_corrected ratio];
    x_out.i=x_regions.i; 
    x_out.v=['areas, baseline-corrected';'area ratio, corrected/not corrected'];
    x_out=div(x_out);
    

endfunction
    
    
