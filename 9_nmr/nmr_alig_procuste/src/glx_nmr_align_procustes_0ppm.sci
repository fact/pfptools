   function x_out0=glx_nmr_align_procustes_0ppm(x_in0,range_nbr_points)

      

            // x_in0 est une matrice de spectres en colonne : 105001 x n

            // x_in est le i° spectre de x_in0
            // x_in est un spectre de 105001 variables; en colonne
            //             début = -0,5 ppm  fin = 10,0 ppm  pas = 0,0001 ppm
            // alignement:  par décalage du pic de TMSP mis sur 0
            //              ppm=0   -> 5001° variable

            // fonction issue de: script_align_jc_11jan24.sci
            // objectif de calage:   ppm=0   sur la variable 5001
            // utilise: simufilters_xl.sci

            // n° de variables pour ppm=0 et target_ppm (ex:4,8)
            ppm0=strtod(x_in0.i);
            c=find(ppm0==min(abs(ppm0)));                                // c=5001 doit correspondre à ppm=0

            // pour éviter des bugs si plusieurs points trouves
            c=c(1);

            // référence pour simufilters_xl
            n_points=size(x_in0.i,1);                                    // ex: 105001
            xref=x_in0(:,1)';
            data_point=ppm0(c+1)-ppm0(c);                                //écart entre 2 points
            xref.d=ones(1,n_points)*2*data_point;                         // 2x l'écart entre 2 points
            xref=div(xref);

            x_out0=x_in0;
            x_out0.d=0*x_out0.d;

           n=size(x_in0.d,2);
            for i=1:n;
                x_in=x_in0(:,i);

                plage1=[c-range_nbr_points; c+range_nbr_points];            // [4001,6001]

                a0=find(x_in.d(plage1(1):plage1(2),:)==max(x_in.d(plage1(1):plage1(2),:)));
                a0=a0(1);                                                   // si plusieurs points trouves
                a1=plage1(1)+a0-1;                                          // ex: 4784

                // augmentation des données pour anticiper le décalage
                x_before.i=string([-1:data_point:-0.5-data_point]');                    // 5000 points à gauche
                n_before=size(x_before.i,1);
                x_before.d=ones(n_before,1)*mean(x_in.d(1:100));
                x_before.v=x_in.v
                x_before=div(x_before);

                x_after.i=string([10.0000+data_point:data_point:10.5]');                   // 5000 points à droite
                n_after=size(x_after.i,1);
                x_after.d=ones(n_after,1)*mean(x_in.d($-100:$));
                x_after.v=x_in.v
                x_after=div(x_after);

                x2=[x_before;x_in;x_after];

                // ajustement de ppm=0 par décalage
                diff_var=a1-c;             // c=5001
                x3=x2;
                x3.i=string(strtod(x2.i)-diff_var*data_point);     // ppm0 bien placé ; 5000 et 5001 pris au hasard dans le>

                ppm_temp=abs(strtod(x3.i));
                x_tmsp=find(ppm_temp==min(ppm_temp));


                // sortie:
                xtemp=x3.d(x_tmsp-c:$,:);
                xtemp=xtemp(1:n_points,:);

                x_out0.d(:,i)=xtemp;

            end


    endfunction
