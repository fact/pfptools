function x=simufilters_xl( sig00,bandpass,width );

  // avec boucle ligne 37 pour grandes dimensions 

  // gestion des données en entrée
  if typeof(sig00)~='div' then
      error('the signal should be a div structure')
  end

  // gestion des filtres de sortie
  if typeof(bandpass)~='div' then
      error('the filters should be a div structure')
  end

  sig=sig00.d;
  [n,nsig]=size(sig);

  // gestion des longueurs d'onde d'entrée
  lam=strtod(sig00.v);
  if find(isnan(lam))~=[] then
      error('please verify that the labels of the signal variable are wavelengths, represented by numbers, without strings')
  end
  // extension gauche / droite du domaine

  // gestion des filtres de sortie
  lout=strtod(bandpass.v);
//  pause
  nf=length(lout);
  if find(isnan(lout))~=[] then
      error('please verify that the labels of the filter variable are wavelengths, represented by numbers, without strings')
  end

  // génération des filtres
  // flt=zeros(nf,nsig);     // supprimé le 13jan24
  x.d=zeros(n,nf);
  x.i=sig00.i;
  for i=1:nf;
     flt_i=exp( -4*log(2)*( (lam-lout(i))/bandpass.d(i) ).^2 )';
     s = sum(flt_i);
     if s > 0 then
        flt_i=flt_i/s;
     end;
     x.d(:,i)=sig00.d*flt_i'; 
  end;

  // flt: 105001 x 131072
  // nf:   105001
  // nsig: 131072
  // flt_i: 1 x 131072
  
  // sig: 3 x 131072
  // flt': 131072 x 105001



  //calcul du signal
  //x=div(sig*flt')
  //x.i=sig00.i;
  x.v=string(lout);

  x=div(x);
//x=flt;

  //mise en forme du filtre
  //flt_out=div(flt);
  //flt_out.i=string(lout);
  //flt_out.v=sig00.v;




endfunction
