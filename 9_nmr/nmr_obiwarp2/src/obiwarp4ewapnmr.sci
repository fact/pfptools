
    function x_out=obiwarp4ewapnmr(x_m,x_s,derivative,response,method,factor1,factor2,gaps1,gaps2)
    
        // x_m et x_s:      divs de dimensions (105001 x1) et (105001 x 1) respectivement 
        // method:      euc = euclidien
        //              cor = correlation
        //              cov = covariance                        
        //              prd = produit scalaire
        // global/local (argument 4 de la fonction): fixé à global=1 [local=0]
        // factor1:     local weighting - diagonal moves
        // factor2:     local weighting - gap moves
        // gaps1:       gap penalty - initiate
        // gaps2:       gap penalty - extend
        // response:    0=linéaire    100=très flexible
    
  
        // arguments d'entree par défaut 
 
        
        if argn(2)<9 then
            gaps2=5;       
        end
        
        if argn(2)<8 then
            gaps1=0;      
        end
        
        if argn(2)<7 then
            factor2=1;      
        end
         
        if argn(2)<6 then
            factor1=2;
        end    
        
        if argn(2)<5 then
            method='euc';
        end
             
        if argn(2)<4 then
            response=10;       // plutôt rigide
        end 
        
        if argn(2)<3 then
            derivative=0;       // lissage sans dérivée
        end
        
        // suppression des valeurs négatives en dessous de -1
        x_m.d(x_m.d<-1)=0;
        x_s.d(x_s.d<-1)=0;
    
        // verification de la taille des spectres master et slave
        n1=size(x_m.d,1);
        n2=size(x_s.d,1);
        if n1 ~= n2 then
           error("obi4ewapnmr2: master and slave not of the same size")
        end;  
    
        // dérivée (0,1 ou 2) 
        xd_m=savgol_xl(x_m',21,derivative);
        xd_m=xd_m';
        xd_s=savgol_xl(x_s',21,derivative);
        xd_s=xd_s'; 
    
        // découpage du calcul par sections de 25000 max; en dessus: ça plante
        xd_A_m=xd_m(1:23750+1000,:);      // des div        // 24750  //attendu  23750
        xd_B_m=xd_m(23751-1000:35600+1000,:);               // 13850             11850        
        xd_C_m=xd_m(35601-1000:52600+1000,:);               // 19000             17000   
        xd_D_m=xd_m(52601-1000:67000+1000,:);               // 16400             14400
        xd_E_m=xd_m(67001-1000:90000+1000,:);               // 25000             23000
        xd_F_m=xd_m(90001-1000:105001,:);                   // 16001             15001       
                                                 // total   //115001            105001
      
        xd_A_s=xd_s(1:23750+1000,:);      // des div
        xd_B_s=xd_s(23751-1000:35600+1000,:);
        xd_C_s=xd_s(35601-1000:52600+1000,:);
        xd_D_s=xd_s(52601-1000:67000+1000,:);
        xd_E_s=xd_s(67001-1000:90000+1000,:);
        xd_F_s=xd_s(90001-1000:105001,:);
      
        // sorties d'obiwarp: les nouveaux déplacements ppm
        ppm_A_s_obi=obiwarp(xd_A_m,xd_A_s,method,1,[],[factor1,factor2],[gaps1,gaps2],[],response); 
        ppm_B_s_obi=obiwarp(xd_B_m,xd_B_s,method,1,[],[factor1,factor2],[gaps1,gaps2],[],response); 
        ppm_C_s_obi=obiwarp(xd_C_m,xd_C_s,method,1,[],[factor1,factor2],[gaps1,gaps2],[],response); 
        ppm_D_s_obi=obiwarp(xd_D_m,xd_D_s,method,1,[],[factor1,factor2],[gaps1,gaps2],[],response); 
        ppm_E_s_obi=obiwarp(xd_E_m,xd_E_s,method,1,[],[factor1,factor2],[gaps1,gaps2],[],response); 
        ppm_F_s_obi=obiwarp(xd_F_m,xd_F_s,method,1,[],[factor1,factor2],[gaps1,gaps2],[],response); 
        
        // regroupement des sections 
        ppm_corrected=[ppm_A_s_obi(1:23750,:); ...
                       ppm_B_s_obi(1001:12850,:); ...
                       ppm_C_s_obi(1001:18000,:); ...
                       ppm_D_s_obi(1001:15400,:); ...
                       ppm_E_s_obi(1001:24000,:); ...
                       ppm_F_s_obi(1001:16001,:)]; 
        
        // sortie 
        x_out=x_s;
        x_out.i=string(ppm_corrected);
        
     
    endfunction
