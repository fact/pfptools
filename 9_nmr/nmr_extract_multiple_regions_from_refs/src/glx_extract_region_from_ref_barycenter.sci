function [x_out_region,x_norm_cos]=glx_extract_region_from_ref_barycenter(xin0,x_reference_region0, norm_choice)
    
        // xin0: spectres RMN, n x q, 105001 points, -0,5:10ppm 
        // x_reference_region0: le spectre du composé de référence dans la région choisie; p x 1 
        
        // x_out_region: la sous-région de xin correspondant à x_reference_region
        // x_infos: contient 3 colonnes
        //     - xin_norm: la norme des spectres de xin; n x 1
        //     - cosinus_ref: le cosinus de chaque spectre de x_out_region avec x_reference  
        
        // pour tester: xmal_A_prealigne_1mars24.tab dans "essais_pls_LNA1_50"  -> xmal_A_prealigne     1999 x 1
        //              xmal_noesy_A.tab                                        -> xA                   56 x 1999
        // [x_out_region,x_norm_cos]=glx_extract_region_from_ref(xA,xmal_A_prealigne)
    
        xin=div(xin0);
        x_reference_region=div(x_reference_region0);
        if size(x_reference_region.d,1) ==1 then
            x_reference_region=x_reference_region';
        end

        // possibilité de tester plusieurs normalisations  13mars24
        if argn(2)<3 then 
            norm_choice="norm1";                // norme calculée sur tout le spectre non nul (105001 points)
        end;
    
        // dimensions
        [n,q]=size(xin.d);
        [p,k]=size(x_reference_region.d);
        if k ~=1 then
            error('dimension error in glx_extract_region_from_ref')
        end
    
        // suppression des valeurs négatives dans xin
        xin_raw=xin;
        xin.d(xin.d<0)=0;
    
        // détermination de la norme                    // 13mars24
        if typeof(norm_choice)=="string" then           // chaine de caractères
            if norm_choice=="none" then
                xin_norm=ones(n,1);                     // ones(n,1)
            elseif norm_choice=="norm0" then            // normes des spectres initiaux avec valeurs negatives
                prod_temp=xin_raw.d*xin_raw.d';
                xin_norm=sqrt(diag(prod_temp));         // vecteur (n x 1)
            elseif norm_choice=="norm1" then            // norme des spectres initiaux avec valeurs positives
                prod_temp=xin.d*xin.d';
                xin_norm=sqrt(diag(prod_temp));         // vecteur (n x 1)
            elseif norm_choice=="norm_zgpr" then        // "norm_zgpr" norme sans compter l'eau
                xin_temp=xin;
                xin_temp.d(:,52501:54001)=0;
                prod_temp=xin_temp.d*xin_temp.d';
                xin_norm=sqrt(diag(prod_temp));         // vecteur (n x 1)
            elseif norm_choice=="norm_noesy" then       // "norm_noesy" sans eau ni etoh
                xin_temp=xin;
                xin_temp.d(:,16651:16851)=0;
                xin_temp.d(:,41401:41601)=0;
                xin_temp.d(:,52501:54001)=0;
                prod_temp=xin_temp.d*xin_temp.d';
                xin_norm=sqrt(diag(prod_temp));         // vecteur (n x 1)
            elseif norm_choice=="area_zgpr" then        // "area_zgpr"
                xin_temp=xin;
                xin_temp.d(:,52501:54001)=0;
                xin_norm=sum(xin_temp.d,'c');           // vecteur (n x 1)
            elseif norm_choice=="area_noesy" then       // "area_noesy"
                xin_temp=xin;
                xin_temp.d(:,16651:16851)=0;
                xin_temp.d(:,41401:41601)=0;
                xin_temp.d(:,52501:54001)=0;
                xin_norm=sum(xin_temp.d,'c');           // vecteur (n x 1)
            end;
        else                                            // pas une chaine de caractères
            xtemp=div(norm_choice);                     // accepte vecteur ou div
            xin_norm=xtemp.d;
            if (size(xin_norm,1)~=size(xin.i,1)) |(size(xin_norm,2)~=1) then
                error('extract_region_from_ref: wrong number of observations or columns');
            end;
        end;

        // extraction de x_out_region
        x_out_region=glx_adjust_barycenter(xin',x_reference_region);
        x_out_region=x_out_region';    
        
//        ppm_xin=strtod(xin.v);
//        ppm_ref=strtod(x_reference_region.i);
//        diff_temp_1= abs(ppm_xin-ppm_ref(1));
//        diff_temp_2= abs(ppm_xin-ppm_ref($));
//        tri1=find(diff_temp_1==min(diff_temp_1));
//        tri2=find(diff_temp_2==min(diff_temp_2));
//        tri1=tri1(1);
//        tri2=tri2(1);
//       
//        x_out_region=xin(:,tri1:tri2);
      
        if size(x_out_region.d,2)~=p then
            error('dim error in glx_extract_region_from_ref')
        end;
        
        // calcul des cosinus
        prod_temp2=x_out_region.d*x_out_region.d';
        diag_temp2=diag(prod_temp2);
        x_out_norm=sqrt(diag_temp2);
        
        norm_ref=sqrt(x_reference_region.d'*x_reference_region.d);
     
        cosinus_nominateur=x_out_region.d*x_reference_region.d;     // numérateur = produit scalaire; n x 1
        cosinus_denominateur=x_out_norm*norm_ref;                   // dénominateur = produit des normes; n x 1
        
        cosinus_ref=cosinus_nominateur ./ cosinus_denominateur;
        
        // regroupement des 2 colonnes xin_norm et cosinus_ref dans un seule matrice
        x_norm_cos.d=[xin_norm cosinus_ref];
        x_norm_cos.i=xin.i;
        x_norm_cos.v=['norm full spectrum';'cosinus_before_aligment'];
        x_norm_cos=div(x_norm_cos);
    
    
endfunction
