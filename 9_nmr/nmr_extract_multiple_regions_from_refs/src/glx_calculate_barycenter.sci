function indice=glx_calculate_barycenter(x)
    
    // x: vecteur colonne, dimensions (p x 1)
    // indice: valeur du barycentre, entre 1 et p
    
    x=div(x);
    if size(x.d,1)==1 then
        x=x';
    end

    q=size(x.d,1);
    q2=floor(q/2);
    
    base=[-q2:q2]';
    matrix_base=base*ones(1,2*q2+1);   // dimensions (2*q2+1,2*q2+1)
    matrix_base2=matrix_base-ones(2*q2+1,1)*base';
    matrix_base2=matrix_base2(1:q,1:q);
    
    matrix_barycenter=x.d'*matrix_base2;
    matrix_barycenter=abs(matrix_barycenter);
    
    indice=find(matrix_barycenter==min(matrix_barycenter));
    indice=indice(1);  // si 2 réponses ou plus 
    
    
endfunction
