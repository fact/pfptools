
function [xout,tri]=glx_adjust_barycenter(xspectrum,xref)
    
    // xspectrum:   (p x n)     p=105001
    // xref:        (q x 1)
 
    // xout:        (q x n)
 
    n=size(xspectrum.d,2);
    q=size(xref.d,1);
    x0_ref=strtod(xref.i(1));       // première valeur ppm de xref
    
    // extraction de la plage de xspectrum correspondant à xref
    ppm=strtod(xspectrum.i);
    ppm_diff=abs(ppm-x0_ref);
    tri=find(ppm_diff==min(ppm_diff));
    
    // première extraction de xspectrum 
    xsp=xspectrum(tri:tri+q-1,:);
 
    // calcul des barycentres 
    bary_xref=glx_calculate_barycenter(xref);
    
    xout.d=[];
    xout.i=xref.i;
    xout.v=xspectrum.v;
    for i=1:n;
        bary_i=glx_calculate_barycenter(xsp(:,i));
        disp(bary_xref,bary_i)
        ppm_i=tri - (bary_xref-bary_i);
        if xout.d==[] then 
            xout.d=xspectrum.d(ppm_i:ppm_i+q-1,i);
        else
            xout.d=[xout.d xspectrum.d(ppm_i:ppm_i+q-1,i)];
        end  
    end
 
    xout=div(xout);
 
endfunction
