
    function [x_shims]=glx_extract_shims(list_rmn_spectra_names)
        
        
        // extrait et regroupe les shims zgpr de plusieurs spectres RMN 1H 
    
        // note: shims zgpr = shims noesy = pas refaits
    
         n=size(list_rmn_spectra_names,1);
         
         x_shims_zgpr.d=zeros(36,n);
         x_shims_zgpr.i=string(zeros(36,1));
         x_shims_zgpr.v=list_rmn_spectra_names;
         for i=1:n;
             x_shims_zgpr.v(i)=strsubst(x_shims_zgpr.v(i),'./temp//','');
         end
         x_shims_zgpr=div(x_shims_zgpr);
         
         for i=1:n;
           
            // zgpr ---------------
            path_zgpr=list_rmn_spectra_names(i)+'/10/shimvalues';  
            h=mopen(path_zgpr);
            x_shim=mgetl(h);            // un vecteur de chaines de caractères
            mclose(h);
        
            // sélection des valeurs de shim 
            x_shim2=x_shim(8:43,:);         // 36 valeurs 
       
            for j=1:36;
                xtemp=tokens(x_shim2(j,1),' ');
                if i==1;
                    x_shims_zgpr.i(j,i)=xtemp(1);
                end;
                x_shims_zgpr.d(j,i)=strtod(xtemp(2));
            end  
         end

         // sortie 
         x_shims=div(x_shims_zgpr);

      
    endfunction
