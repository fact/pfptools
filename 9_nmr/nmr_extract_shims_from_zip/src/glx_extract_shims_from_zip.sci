function x_shims=glx_extract_shims_from_zip(zip_file)
    
    // maj: 7fev24
    // zip_file:            un fichier .zip contenant un répertoire par spectre RMN  
    // which_directory:     un répertoire temporaire pour décompresser le .zip
    // x_shims:             le fichier de shims                                         (36 x n)
 
    
    // extraction dans un repertoire temporaire
    if argn(2)<2 then
        which_directory='./temp/';
    end
         
    rmdir(which_directory,'s');    // cas ou le repertoire existe deja
    mkdir(which_directory);

    files=decompress(zip_file,which_directory);
     
    // contenu du repertoire
    dir1=dir(which_directory);
    dir_list0=dir1(2);    
   
    // elimination des non=spectres RMN (parfois rajoutes par mac comme _MACOSX)
    dir_list=[];
   
    for i=1:size(dir_list0,1);
        if isdir(which_directory + '/' + dir_list0(i) + '/10/') then 
            dir_list=[dir_list;dir_list0(i)]; 
        end
    end
   
    path_dir=which_directory+'/'+dir_list;
 
    // liste des repertoires et fichiers 
    // note: les repertoires et fichiers n'etant pas des spectres RMN sont automatiquement ecartes
    x_shims=glx_extract_shims(path_dir);
    
    // nettoyage du répertoire temporaire
    rmdir(which_directory,'s');
     
endfunction
