function  [x_deconv,areas,cos_after]=glx_nmr_several_deconvol_orth_proj_v2(x0,ref0,full_spectra_norm,global_norm_coeff,zeroing)
    
    // x0:                une matrice (n x q) 
    // ref0:              un vecteur (1 x q) pré-aligné sur x_in
    // full_spectra_norm: les aires sont divisées par ce vecteur de dimension n, qui est p.ex. la norme du spectre entier (sur 20 ppm)
    // global_norm_coeff: un coefficient unique multiplié à toutes les données
    // zeroing:           l'indice après lequel les valeurs de signal sont utilisées pour calculer la ldb moyenne
    
    // x_deconvol:  les spectres déconvolués de x_in par projection orthogonale; une matrice (n x q)
    // areas    :   les aires sont multipliées par cette valeur, un nombre décimal 
    // cos_after:   cosinus après alignement 
    
    x_in=div(x0);                       // n x q
    s_in=div(ref0);                     // 1 x q
    fsn=div(full_spectra_norm);         // n x 2  ( 1=norme, 2=cosinus)
    
    [nx,qx]=size(x_in.d);
    [ns,qs]=size(s_in.d);
    
    if qx ~=qs then
        error('nmr_deconv_orth_proj:X and ref should have the same number of variables')
    end
    
    if ns ~=1 then
        error('nmr_deconv_orth_proj:ref_in should be a line vector')
    end

    if size(fsn.d,1) ~= nx then
        error('nmr_deconv_orth_proj:not the same number of variables in X and norm(X)')
    end
    
    
    x=x_in.d;           // spectres en ligne
    s=s_in.d';          // vecteur-colonne
 
    // alignement ldb: déplacé le 5 juillet + correction linéaire avec pente possible
    // attention, les spectres sont recopiés 5x 
    // calcul fait sur 1/5° du spectre, puis recopié
    if argn(2)>4 then
        x_ldb5=[];                                      // ldb de 1/5° du spectre
        x_begin=mean(x(:,1:zeroing),'c');
        x_end=mean(x(:,$-zeroing:$),'c');
        x_min=min([x_begin x_end],'c');

        // correction ldb 
        x=x-(x_min*ones(1,qx));    
    end;
    // fin 5juillet-------------
    
    xp=x*s*inv(s'*s)*s';
    
    // sorties
    x_deconv.d=xp;
    x_deconv.i=x0.i;
    x_deconv.v=x0.v;
    x_deconv=div(x_deconv);
    
    // ajustement ligne de base jusqu'au 4 juillet 24:  enlève une ligne de base linéaire basée sur les zeroing derniers points    // et il est mal placé 
//    if argn(2)>4 then
//        x_deconv.d=x_deconv.d-mean(x_deconv.d(:,$-zeroing:$),'c')*ones(1,size(x_deconv.d,2)); 
//    end
    // fin 4juil24 
    
    
    // cas observé: les spectres sont tous négatifs! donc patch pour éviter ce cas 
    // nx et qx
    
    choice_sign=ones(nx,1);
    
    [nul,tri]=find(mean(x_deconv.d,'c')<0);
    
    choice_sign(:,tri)=-1;
    
    x_deconv.d=x_deconv.d .* (choice_sign*ones(1,qx));
    
    
   // pause
    
    
    x_deconv.d(x_deconv.d<0)=0;
      
    areas.d=sum(xp,'c');            // aire 
    areas.d=global_norm_coeff * areas.d ./ fsn.d(:,1);
    areas.i=x_in.i;
    areas.v="areas";
    areas=div(areas);
 
   
    // calcul du cosinus apres alignement 
    inner_product=x_in.d*s_in.d';			// n x 1
    norm_x=sqrt(diag(x_in.d*x_in.d'));
    norm_ref=sqrt(s_in.d*s_in.d');
    cos_after.d=inner_product ./ (norm_ref*norm_x);
    cos_after.i=areas.i;
    cos_after.v=s_in.i;
    cos_after=div(cos_after);

// pause 
 
   
    
endfunction
