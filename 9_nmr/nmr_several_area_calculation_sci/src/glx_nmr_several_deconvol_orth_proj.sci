function  [x_deconv,areas,cos_after]=glx_nmr_several_deconvol_orth_proj(x0,ref0,full_spectra_norm,global_norm_coeff,zeroing)
    
    // x0:                une matrice (n x q) 
    // ref0:              un vecteur (1 x q) pré-aligné sur x_in
    // full_spectra_norm: les aires sont divisées par ce vecteur de dimension n, qui est p.ex. la norme du spectre entier (sur 20 ppm)
    // global_norm_coeff: un coefficient unique multiplié à toutes les données
    // zeroing:           l'indice après lequel les valeurs de signal sont utilisées pour calculer la ldb moyenne
    
    // x_deconvol:  les spectres déconvolués de x_in par projection orthogonale; une matrice (n x q)
    // areas    :   les aires sont multipliées par cette valeur, un nombre décimal 
    // cos_after:   cosinus après alignement 
    
    x_in=div(x0);                       // n x q
    s_in=div(ref0);                     // 1 x q
    fsn=div(full_spectra_norm);         // n x 2  ( 1=norme, 2=cosinus)
    
    [nx,qx]=size(x_in.d);
    [ns,qs]=size(s_in.d);
    
    if qx ~=qs then
        error('nmr_deconv_orth_proj:X and ref should have the same number of variables')
    end
    
    if ns ~=1 then
        error('nmr_deconv_orth_proj:ref_in should be a line vector')
    end

    if size(fsn.d,1) ~= nx then
        error('nmr_deconv_orth_proj:not the same number of variables in X and norm(X)')
    end
    
    
    x=x_in.d;           // spectres en ligne
    s=s_in.d';          // vecteur-colonne
    
    xp=x*s*inv(s'*s)*s';
    
    // sorties
    x_deconv.d=xp;
    x_deconv.i=x0.i;
    x_deconv.v=x0.v;
    x_deconv=div(x_deconv);
    
    // ajustement ligne de base
    if argn(2)>4 then
        x_deconv.d=x_deconv.d-mean(x_deconv.d(:,$-zeroing:$),'c')*ones(1,size(x_deconv.d,2));
    end
    
    x_deconv.d(x_deconv.d<0)=0;
      
    areas.d=sum(xp,'c');            // aire 
    areas.d=global_norm_coeff * areas.d ./ fsn.d(:,1);
    areas.i=x0.i;
    areas.v="areas";
    areas=div(areas);
   
   
    // calcul du cosinus apres alignement 
    inner_product=x0.d*ref0.d';			// n x 1
    norm_x=sqrt(diag(x0.d*x0.d'));
    norm_ref=sqrt(ref0.d*ref0.d');
    cos_after.d=inner_product ./ (norm_ref*norm_x);
    cos_after.i=areas.i;
    cos_after.v=ref0.i;
    cos_after=div(cos_after);
   
    
endfunction
