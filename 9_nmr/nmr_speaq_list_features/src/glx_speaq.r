#Auteur: JC Boulet
# Entrée: x7_master et x2_slave sont des structures div (5 champs)  (n x p) et (1 x p)
# Sortie: x1_slave_align3 est un tabular
# Librairies: speaq, Rmatlab, data.table


glx_speaq <- function(x7_master,x2_slave,bl_thresh) {
    
    ## ---------------------------------
    ## Identification des pics de Master
    ## ---------------------------------
    col_labels=as.double(unlist(x7_master[,1]));
    col_labels=t(col_labels);
    col_names=names(x7_master);

    x7_master2=as.double(unlist(x7_master[,2]))
    x7_master2=t(x7_master2);

    peakList_x7=detectSpecPeaks(x7_master2,nDivRange=c(128),scales=seq(1,16,2),baselineThresh=bl_thresh,SNR.Th=-1,verbose=FALSE);

    resFindRef = findRef(peakList_x7)
    refInd = resFindRef$"refInd";
    tarInd=1;

    refSpec=x7_master2[refInd,];
    tarSpec=x7_master2[tarInd,];

    mergedPeakList=c(peakList_x7[[refInd]],peakList_x7[[tarInd]]);
    mergedPeakLabel=double(length(mergedPeakList));
    for (i in seq_along(peakList_x7[[refInd]]) ) {
        mergedPeakLabel[i]=1;
        startP=1;
        endP=length(tarSpec);
    }

    
    ## --------------------------------------------------
    ## recuperation des parametres d'alignement de master
    ## --------------------------------------------------
    
    peak_name=seq(1,length(mergedPeakList),1);
    mergedPeakListLabel=rbind(peak_name,mergedPeakList,mergedPeakLabel);

    mergedPeakListLabel2=cbind(rbind('name','mergedPeakList', 'mergedPeakLabel'),mergedPeakListLabel);
          
          
    ## ------------------------------
    ## Alignement de Slave sur Master
    ## ------------------------------
    x1_slave2=x2_slave[,-1];
    x1_slave2=as.matrix(x1_slave2,rownames=FALSE);
    nspectra=ncol(x1_slave2);
    x1_slave2=t(x1_slave2);
    x1_slave_align0=0*x1_slave2;

    for (i in seq(1,nspectra,1)) {
      xi=x1_slave2[i,];
      xi=t(xi);
      res=hClustAlign(x7_master2,xi,mergedPeakList,mergedPeakLabel, startP, endP, maxShift=50, acceptLostPeak=TRUE);
      x1_slave_align0[i,]=res$"tarSpec";
    }

    x1_slave_align=rbind(col_labels,x1_slave_align0);
    x1_slave_align2=t(x1_slave_align);
    x1_slave_align3=rbind(names(x2_slave),x1_slave_align2);
    
    return(x1_slave_align3)
    
}
