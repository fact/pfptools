function xout=glx_repeat_structures(xin,k)
    

        xlist=fieldnames(xin);
        
        n=size(xlist,1)-1;
        
        xout=struct();
        
        for i=1:n;
            execstr(msprintf("xtemp=xin.list%s;",string(i)));  
            xtemp2=xtemp;
            for j=2:k;
                xtemp2.d=[xtemp2.d;xtemp.d];
            end;
            nvar=size(xtemp2.d,1);
            xtemp2.i=string([1:nvar]'); 
            xtemp2.i=justify(xtemp2.i,'r'); 
          
            execstr(msprintf("xout.list%s=xtemp2",string(i)));
        end
        
        xout.typeof="list";
    
endfunction
