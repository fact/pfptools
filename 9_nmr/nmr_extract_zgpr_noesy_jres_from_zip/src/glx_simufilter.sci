function x=glx_simufilter(sig00,bandpass,width );

  // sug00:    matrice (n x p)
  // bandpass: vecteur (1 x q)

  // gestion des données en entrée
  if typeof(sig00)~='div' then
      error('the signal should be a div structure')
  end

  // gestion des filtres de sortie
  if typeof(bandpass)~='div' then
      error('the filters should be a div structure')
  end
  
  if size(bandpass.d,2)==1 then
    bandpass=bandpass';                                  // 2jan24 pour avoir un vecteur-ligne 
  end

  sig=sig00.d;
  [n,nsig]=size(sig);

  // gestion des longueurs d'onde d'entrée
  lam=strtod(sig00.v);
  all_nan=find(isnan(lam));
  if all_nan ~=[] then 
       error('please verify that the labels of the signal variable are numbers, without strings');
  end;
  // extension gauche / droite du domaine

  // gestion des filtres de sortie
  lout=strtod(bandpass.v);

  nf=length(lout);
  if find(isnan(lout))~=[] then
      error('please verify that the labels of the filter variable are numbers, without strings');
  end


  // génération des filtres
 // flt=zeros(nf,nsig);
  x.d=zeros(n,nf);
  x.i=sig00.i;

  for i=1:nf;
     flt_i=exp( -4*log(2)*( (lam-lout(i))/bandpass.d(i) ).^2 )';
     s = sum(flt_i);
     if s > 0 then
        flt_i=flt_i/s;
     end;
     x.d(:,i)=sig00.d*flt_i'; 
  end;

  x.v=string(lout);

  x=div(x);



endfunction
