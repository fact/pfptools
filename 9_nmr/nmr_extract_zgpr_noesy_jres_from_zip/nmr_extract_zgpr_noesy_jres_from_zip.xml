<tool id="2024_01_nmr_extract_from_zip" name="Extraction of zgpr/noesy/jres spectra" version="0.0.1">

<description> from several directories within a zip file   </description>

<requirements>
    <requirement type="package" >scilab</requirement>
</requirements>


<stdio>
    <exit_code range="1:" level="fatal" />
</stdio>


<command> 
 <![CDATA[ 
  if [ -d $__root_dir__/packages/scilab-2024.0.0 ]; then $__root_dir__/packages/scilab-2024.0.0/bin/scilab-cli -nb -quit -f ${script_file}; else $__root_dir__/packages/scilab-2024.0.0/bin/scilab-cli -nb -quit -f ${script_file}; fi 
 ]]>
</command>

  <configfiles>
    <configfile name="script_file">
    <![CDATA[ lasterror(); 
    
        warning('off');
    
        if ~isdef('pls') then 
            atomsSystemUpdate(); 
            atomsInstall('FACT');
            atomsLoad('FACT'); 
        end; 
  
        funcprot(0);  // enlève les messages
       
        exec("$__tool_directory__/src/glx_simufilter.sci",-1); 
        exec("$__tool_directory__/src/glx_extract_zgpr_noesy_jres.sci",-1); 
    
        exec("$__tool_directory__/src/glx_extract_zgpr_noesy_jres_from_zip.sci",-1);     

        lasterror(%f); 
    
        which_directory="$__tool_directory__/tmp";
        
        [x_zgpr,x_noesy,x_jres_sum,x_jres_sum_smooth,x_jres_all]=glx_extract_zgpr_noesy_jres_from_zip("${zip_file}",which_directory);
    
        div2tab(x_zgpr,"${x_zgpr}");
        div2tab(x_noesy,"${x_noesy}");
        div2tab(x_jres_sum,"${x_jres_sum}");
        div2tab(x_jres_sum_smooth,"${x_jres_sum_smooth}");
        save("${x_jres_all}",'x_jres_all');
   
        if ~isempty(lasterror(%f)) then
        write(0,lasterror());
        end; ]]>
</configfile>
  </configfiles>


  <inputs>
    <param name="zip_file"       format="zip"      type="data"      label="Zip file containing several NMR spectral directories"    />
  </inputs>


  <outputs>
    <data name="x_zgpr"                 format="tabular"        label="NMR zgpr"  />
    <data name="x_noesy"                format="tabular"        label="NMR noesy"  />
    <data name="x_jres_sum"             format="tabular"        label="NMR jres_sum"  />
    <data name="x_jres_sum_smooth"      format="tabular"        label="NMR jres_sum_smooth"  />
    <data name="x_jres_all"             format="mat"            label="NMR jres all"  />
  </outputs>
  
  
   <tests>
   
    <test>
       <param name="format_io"    value="0"/>
       <param name="filein"       value="VI2016_AC_4Areduit_1.dat"/>
       <param name="signalthresh" value="0"/>
       <param name="type_sm" value="1"/>
       <output name="resbary"   file="VI2016_AC_4Areduit_1centroide.mat" compare="sim_size" delta="2000" />
    </test>

    <test>
       <param name="format_io"    value="1"/>
       <param name="filein"       value="VI2016_AC_4Areduit_1.h5"/>
       <param name="signalthresh" value="0"/>
       <param name="type_sm" value="1"/>
       <output name="resbary"   file="VI2016_AC_4Areduit_1centroide.mat" compare="sim_size" delta="2000" />
    </test>

  </tests>
  
  
  <help>


**Author**  Jean-Claude Boulet (INRAE).


---------------------------------------------------

=======================================================================================
Extraction of zgpr, noesy and jres from Bruker NMR directories within a zip
=======================================================================================

-----------
Description
-----------

A series of individual NMR spectra is represented by a set of directories.

These directories were previously compressed using zip, then uploaded in Galaxy. 

The NMR spectra will be gathered in a single file for each of the three sequences: zgpr, noesy and jres. 

------
Inputs
------

**Zip file containing several NMR spectral directories**

Spectra, within the .zip file, are supposed to share the following configuration: 

- zgpr and noesy: 131072 datapoints for the ppm scale 
  
- jres: F1=128; F2=16384 datapoints for the ppm scale 
    
- directories names: 10=zgpr, 11=noesy, 12=jres


----------
Parameters
----------

None to be tuned. Parameters have already been set: 

- extrapolation of jres spectra, ppm scale jres -> ppm scale noesy, is performed with a bandwith of 0.001ppm 

- then smoothing of jres spectra (ppm scale=noesy) is performed with Savitzky-Golay, window 21 datapoints (~ 0.003ppm), order=0
 


-------
Outputs
-------

**NMR zgpr spectra**

A matrix of (n x 131072), n zgpr spectra (no signal suppression)
format = tabular

**NMR noesy spectra**

A matrix of (n x 131072), n noesy spectra (water and ethanol removed), 131072 ppm-variables

format = tabular

**NMR jres sum-spectra**

A matrix of (n x 16384). Each spectrum is the sum of the 128 individual spectra 

format = tabular

**NMR jres sum-smooth spectra**

A matrix of (n x 131072). The previous jres sum-spectra have been extrapolated then smoothed to obtain the same number of ppm-variables as zgpr or noesy. 

format = tabular

**NMR jres all**

A matrix of (16384 x 128 x n).  

format = scilab/HDF5, not editable in Galaxy


</help>


<citations>

<citation>
</citation> 

</citations>


</tool>
