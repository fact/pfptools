function x_out=mz_export_uv_orbitrap(x_in, lambda_begin,lambda_range)
    
    // extraction des spectres UV acquis en même temps que le signal m/z
    
    // x_in: spectre Thermo-Orbitrap au format .h5
    // pour mémoire: ce fichier a été obtenu après 2 conversions:
    //  1- .raw -> .mz5 avec MSconvert
    //  2- .mz5 -> .h5 avec HDFview 
    
    // x_out: fichier .csv contenant les labels de ligne + colonne 
    //                      + les spectres UV de en ligne 
    // dimensions: n x p       n=nbre de RT     p=nbre de variables dans le spectre 
    
    

    // nom du fichier de sortie ------------------------------------------------
    xtemp=strsplit(x_in,'.');
    if max(size(xtemp))>2 then
        error("mz_export_uv_orbitrap: a single dot expected in the name")
    end
    x_out_name=xtemp(1)+".csv";
 
    
    // chargement du fichier d'entrée .h5  -------------------------------------   
    a=h5open(x_in,"r","stdio");

    spectrum_chromatogram_index=h5read(a.root.ChromatogramIndex);
    spectrum_chromatogram_index=spectrum_chromatogram_index(1);    // une valeur 
    spectrum_time=h5read(a.root.ChomatogramTime);
    spectrum_intensity=h5read(a.root.SpectrumIntensity);
    spectrum_mz=h5read(a.root.SpectrumMZ);
    spectrum_index=h5read(a.root.SpectrumIndex);  
    // spectrum_cvparam=h5read(a.root.CVParam);    // ne marche pas !              
    h5close(a)
    
    // notes observées sur un exemple: C4_neg_02.h5
    // spectrum_chromatogram_index:     879
    // spectrum_index:                  8079      
        // spectrum_index(1)                1522
        // spectrum_index(8079)             26803535
    // spectrum_intensity:              26803535
    // spectrum_mz:                     26803535   
        // transition ms/uv observée à  25968335  
        // donc 835200 valeurs pour l'UV    
        // début dernier UV:            26803419
        // fin dernière valeur UV:      26803534
        // -> 1 spectre UV =            115 valeurs 
        // -> 8079 spectres UV =        101085 valeurs 
    // spectrum_time:                   8079
    
    // 1 spectre fait 116 valeurs : 190-> 650 par pas de 4 
    
    // idée: trier sur l'occurence 190 / 4 / 4 / 4
    
    // note: structure du fichier texte
    // - spectrum index: 0 (sur 1522 points)
    // ...
    // - spectrum index: 878 (sur 10320 points) mz array 
    //                                              puis intensity array
    // - spectrum index: 879 (sur 116 points) - le premier-
    // ...
    // - spectrum index: 8077 (sur 116 points) : voir ci-dessous
    // - spectrum index: 8078 (sur 116 points): wavelength array 190-650
    //                                              puis intensity array 
    // - TIC (sur 879 points): time array puis intensity array 
    // - spectre d'absorption (sur 7200 points): time array jusqu'à la valeur 24' 
    //                                              puis intensity array 
    // [donc on n'a pas la séquence 190-4-4] 
    // bilan: 0-878 = 879 spectres MS
    //        879-8078= 7200 spectres UV
    
    spectrum_mz=spectrum_mz';
    
    code= [spectrum_mz [spectrum_mz(2:$,:);0] [spectrum_mz(3:$,:);0;0]]; // décalage de l'échelle 
    
    tri=find(code(:,1)==lambda_begin & code(:,2)==lambda_range & code(:,3)==lambda_range);
    tri=tri';
    
    n_spectres=max(size(tri));       // 7200 <-> même valeur que le spectre d'absorption final 
    
    // taille des spectres IR: 
    size_spectre=tri(2)-tri(1); // 116
    
    // calcul des variables spectrales 
    lambda_end=lambda_begin+(size_spectre-1)*lambda_range;
    
    x_out.v=string([lambda_begin:lambda_range:lambda_end]');        // variables spectrales
    
    // récupération des signaux 
    x_out.d=zeros(n_spectres,size_spectre);
    // spectrum_intensity=spectrum_intensity';     // vecteur colonne 
    for i=1:n_spectres;
        xtemp=spectrum_intensity(tri(i):tri(i)+size_spectre-1);
        x_out.d(i,:)=xtemp;
    end
    
    time_begin=spectrum_chromatogram_index+1;   // 879 + 1
    time_end=max(size(spectrum_index));         // 8079
    
    spectrum_time=spectrum_time';               // 8079 x 1
    
    x_out.i=spectrum_time(time_begin:time_end,:);
    x_out.i=string(x_out.i);
    
    x_out=div(x_out);               // x_out.d: 7200 x 116 
                                    // i - temps v = variables du spectre UV
    
    
    // --- export des données en csv ------------------------   
    div2csv(x_out,x_out_name,';',',')
    
    
    
endfunction
